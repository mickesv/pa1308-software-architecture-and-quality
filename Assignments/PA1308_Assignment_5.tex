\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[latin1]{inputenc}
%\usepackage[swedish]{babel}
\usepackage{hyperref}

\newcounter{Quest}
\setcounter{Quest}{1}
\newcommand{\question}[1]{\vspace{12pt} \noindent \emph{Q\theQuest\refstepcounter{Quest}. #1.}}

\title{PA1308 Assignment 5}
\author{Mikael Svahnberg\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
Mikael.Svahnberg@bth.se}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
\label{S:Introduction}
The purpose of this assignment is for individual reflection, both upon the previous assignments, but also on software architecture in general. A number of questions are asked, and you are expected to, \emph{individually}, compile a report with answers to them and submit on the course homepage on or before the specified date and time.

\subsection{Viva}
Please be advised that you \emph{may} be called upon to orally defend your
answers in your report.

\subsection{Collaboration Policy}
In order to evaluate you on this assignment, we must be sure that your work is your own. Therefore:

\begin{itemize}
\item You may discuss the questions in Section \ref{S:CA} with your team-mates, e.g. in order to help you remember what happened in your project.
\item The subsequent analysis must, however, be your own. You must write your report yourself.
\item You may discuss the systems in Section \ref{S:AR} with anyone, in order to
understand the systems and in order to understand the description of the
systems. 
\item You may \emph{not} discuss your answers to the questions in Section \ref{S:AR} with anyone except the teachers in this course. Your analysis must be your own. You must write your report yourself.
\end{itemize}

\subsection{Grading}
%Grading is done as follows. First, each question is marked as
%``incomplete''=0p, ``partial''=0.5p, or ``satisfactory''=1p. Second, your
%points are summed up, and a grade is given according to Table
%\ref{T:GradeLimits}.

% More detailed grading attempt below, based on MUN feedback:
Grading is done as follows. For the answer to a question to be considered
\emph{satisfactory}, all parts (including sub-questions) should show reflective
insight that goes beyond recreation of facts occurred during the project, or
goes beyond what is stated in the material used to answer the question (e.g.
books). Motivations for each part of the answer are provided (and are reasonable
and realistic), and alternatives (where applicable) are at least mentioned. Each
question is assessed according to this ``gold standard'' as follows:

\begin{itemize}
\item[(1p)] \emph{Satisfactory}: All parts of the question are answered satisfactory.
\item[(0.7p)] \emph{Partial} Less than $\frac{2}{3}$ of the question (including sub-questions) are answered satisfactorily. The main question is answered sufficiently, but not completely.
\item[(0.4p)] \emph{Incomplete} Less than $\frac{1}{2}$ of the question (including sub-questions) are answered satisfactorily. The main question is not answered satisfactorily.
\item[(0p)] \emph{Insufficient} The question is not answered at all (blank) or the statements are not relevant for the stated question. 
\end{itemize}

Your points are then summed up, and a grade is given according to Table \ref{T:GradeLimits}.

\begin{table}[bth]
  \centering
  \begin{tabular}{rl}
    Limit & Grade\\
    \hline
    $<60\%$ & F\\
    $60-64\%$ & E\\
    $65-69\%$ & D\\
    $70-79\%$ & C\\
    $80-89\%$ & B\\
    $\geq 90\%$ & A\\
    \hline
  \end{tabular}
  \caption{Grade Limits}
  \label{T:GradeLimits}
\end{table}

\section{Questions}
\label{S:Questions}
\subsection{Course Assignments}
\label{S:CA}
In this section, we ask you to reflect upon the assignments your team submitted,
and your contribution to them. You may wish to read up on ``postmortem
documentation'' (e.g., Collier et al. \cite{Collier:1996}), and chapter 12 in
Hofmeister et al.\cite{Hofmeister:2000} (\emph{``The role of the software architect''}).

\question{Vision} Please describe the process by which you created your original architecture vision (i.e. a global view and a general idea of what the system will look like when you are done).

\question{Process Decisions} Please identify your three most important decisions
with respect to your architecture development \emph{process}. How did these
decisions influence the way you worked? What impact did they have on the final
results?

\question{Product Decisions} Please identify your three most important architectural \emph{product} decisions. How did these decisions influence your architecture? What impact did these decisions have on your workload?

\question{Product Quality}
\begin{enumerate}
\item What parts of your architecture do you consider to be ``best'' (i.e. has the highest quality, meets the requirements best, and is the most robust)? Why?
\item Likewise, what are the weakest parts of your architecture? Why?
\end{enumerate}

%MUN: What do we mean by ``plan'' and ``outcome''? They were not supposed to
%have a ``plan'' as in project management lingo. Do you rather mean
%``expectations'' vs. ``actual result''. Still, there is the question of what?
%The architecture of the system after reading its description, or the course
%itself, 
\question{Process Expectations vs. Result} Please describe the three most severe deviations between your expectations of how the assignment work should have progressed and how it actually progressed. Why did these deviations occur? What could \emph{you} have done differently to reduce these deviations\footnote{We are
already aware of the too tight feedback loops and the considerable re-work you
needed to do from one assignment to the other, so please identify three
\emph{other} causes.}?

\question{Product Expectations vs. Results} Please describe the three largest deviations between your expectations of how the architecture should look like and your actual results. What could you have done differently to reduce these deviations?

\question{Individual contribution} Please assess your individual contribution to the architecture. Did you do enough? Did you do less than the rest of the team? Did you do more than the rest of the team? Why? What could you have done differently?

%MUN s/enthusiasmise/motivate ??
\question{Coaching} What did you do to coach and motivate the rest of the team in their work? Was this sufficient? Why or why not?

\subsection{Architecture Reflection}
\label{S:AR}
This set of questions rely on the book ``The architecture of Open Source Applications'', available online at \url{http://www.aosabook.org/en/index.html}. Specifically, we ask questions from two chapters; Volume I, chapter 17, \emph{``Sendmail''} by Eric Allman, and Volume II, chapter 14, \emph{``nginx''} by Andrey Alexeev.

\question{Sendmail Issues and Strategies}
\begin{enumerate}
\item Please recreate the five most important issues and their corresponding strategies for sendmail. You do not need to create full issue cards; describing the issues and the strategies are sufficient.
\item Why do you consider these to be the most important?
\item Please relate the identified strategies to tactics proposed in chapter 5
of Bass et al.\cite{Bass:2012}.
\end{enumerate}

\question{Recreate Sendmail Architecture Views}
\begin{enumerate}
\item Would you be able to recreate a conceptual view, a module view, or an execution view based on the information in the sendmail chapter?
\item What information or lack of information in the chapter makes this possible or impossible?
\end{enumerate}

\question{nginx Issues and Strategies}
\begin{enumerate}
\item Please recreate the five most important issues and their corresponding strategies for nginx. You do not need to create full issue cards; describing the issues and the strategies are sufficient.
\item Why do you consider these to be the most important?
\item Please relate the identified strategies to tactics proposed in chapter 5
of Bass et al.\cite{Bass:2012}.
\item How would you prioritise among the issues identified for nginx? Please motivate your priorities.
\end{enumerate}

\question{nginx Architectural Views}
\begin{enumerate}
\item From which architectural perspective (view) is nginx discussed? Please also present how you come to this conclusion.
\item Why do you think they focus on this view?
\end{enumerate}


% Recreate issues and strategies for sendmail
% discuss how they change as a consequence of changing reality
% relate to tactics from Bass et al. wherever possible
% Can you create a reasonable architecture based on this information? WHy? Why not?

% Identify issues for nginx
% Discuss priority among issues for nginx
% From which architectural view of nginx discussed. Why this view?


\bibliographystyle{abbrv}
\bibliography{/Users/msv/Documents/Research/references/references.bib}

\end{document}
