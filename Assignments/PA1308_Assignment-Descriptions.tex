\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{cite}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[utf8x]{inputenc}
%\usepackage[swedish]{babel}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}
\usepackage{enumerate}

\title{PA1308 Assignment descriptions}
\author{Michael Unterkalmsteiner, Nauman bin Ali, Mikael Svahnberg\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
\{mun,nal,msv\}@bth.se}
%\date{}                  % Activate to display a given date or no date

\begin{document}
\maketitle
\begin{abstract}
This document describes the five assignments in the course PA1308 Software
Architecture and Quality, autumn 2012 edition, offered at Blekinge Institute of 
Technology. 
\end{abstract}

\section{Introduction}
At the start of the course, you are expected to form groups of four.
As a group, you work on assignments 1, 3, 4 and 5. On assignment 2 you work
individually. You will receive feedback (written for groups, and
addressing general challenges during the lectures) on your submissions. You
shall consider this feedback when preparing the next assignment or
re-submissions.

The goal of assignments 1, 3 and 5 is to practice a controlled process for
creating and documenting a software architecture, with an understanding of how
to transform quality goals into a practical architecture solution, and to apply
appropriate architecture styles and patterns in this process. The design method
by Hofmeister et al. must be used when designing and documenting the
architecture. The architecture must be evaluated with respect to relevant
quality requirements and, if necessary, transformed until the quality
requirements are met. 

Assignment 2 (individual) consists of a set of questions where you are
encouraged to reflect on what you have been taught during the lectures
and your practical experiences.

The goal of assignment 4 is to introduce a formal language for architecture
specification and evaluation.

\newpage
\section{Assignment 1: Architecture Decisions}
\begin{quote}
System analysis and architectural decisions
\end{quote}

\noindent You will be provided with a system description on the course
homepage. This document contains a general description of the system and its
intended use. Based on this information, you are supposed to extract
requirements and define additional requirements if necessary.

You are then expected to construct factor tables, issue cards, and relevant
strategies for solving the issues. Bear in mind that the strategies must
primarily be \emph{software}-related, and wherever you use tactics from Bass et
al. or any particular architecture style, this shall be properly referenced.
All your strategies need to be well motivated in order to create a clear link 
between the issue itself and how the strategy addresses the issue.

Finally, you are expected to create a conceptual view of the system, where 
relevant strategies are implemented, \emph{with} traceability links back to the
relevant issue cards.

\subsection{Assignment Analysis Tasks}
\begin{enumerate}
\item (individual) Work through the relevant study packages.
\item Read and analyse the system description.
\item Write an assignment introduction, presenting the assignment and the system.
\end{enumerate}

\subsection{Central Assignment Tasks}
\begin{enumerate}
\item Create and motivate a preliminary architectural style. Document this.
\item Create factor tables. Use Hofmeister et al., page 32 to identify relevant factors.
\item Create Issue cards and strategies. Use Hofmeister et al., page 32 to identify relevant issues. Make sure your strategies address the issue, and do so by solving the problem in the software architecture.
\item Identify which issues and strategies that should be addressed in the conceptual view.
\item Implement your identified strategies on your preliminary architectural style. Document where and how each strategy is implemented. Document any overall changes you need to do on the preliminary architecture style.
\item Document your assumptions.
\end{enumerate}

\subsection{Final Assignment Tasks}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
 \item Find a group with which you perform the peer evaluation. Exchange the
deliverables of assignment 1 AT LATEST 3 days before the peer evaluation
seminar.
\end{enumerate}

\subsection{Deliverables}
One PDF document containing:
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Introduction}] An introduction, presenting the assignment and the
system.
 \item [{Assumptions}] A description of assumptions you have on the nature of
the application domain, the system, the environment in which it will be
deployed, the customer and users.
 \item [{System analysis}] Factor tables with relevant factors (organizational
factors left out), issue cards and strategies.
 \item [{Conceptual View}] The analysis and design of the architecture according
to Hofmeisters' conceptual view. Motivate design choices by the developed
strategies!
\end{description}

\subsection{Relevant study packages}
Relevant study packages include, but may not be limited to:
\begin{itemize}
 \item Architecture Fundamentals
 \item Architecture Documentation
\end{itemize}

\subsection{Tools for drawing architecture views}
Hofmeister uses UML notation for their different architectural views. Therefore, 
we suggest you to use an UML drawing tool to create the diagrams for your 
architecture. If you have already used an UML tool, we suggest you to continue 
using it in order to reduce your learning curve and effort. Otherwise, here is
a selection of tools you may use:
\begin{itemize}
 \item Modelio (www.modelio.org)
 \item UMLet (www.umlet.com)
 \item Visual Paradigm for UML (www.visual-paradigm.com)
 \item Gaphor (gaphor.sourceforge.net)
\end{itemize}

\newpage
\section{Assignment 2: Personal Reflections}
\begin{quote}
Individual Analysis of an Open Source Architecture
\end{quote}
% Find exam questions, come up with some new ones...

The purpose of this assignment is for individual reflection. You are expected to, \emph{individually}, compile a report with answers to them and submit on the course homepage on or before the specified date and time. Please be advised that you \emph{may} be called upon to orally defend the answers in your report.

\subsection{Collaboration Policy}
In order to evaluate you on this assignment, we must be sure that your work is your own. Therefore, you may \emph{not} discuss your answers to the questions with anyone except the teachers in this course. Your analysis must be your own. You must write your report yourself. You may use other sources than the ones described if properly referenced, but the analysis and the actual report must still be your own creation.

\subsection{Grading}
Grading is done as follows. For the answer to a question to be considered
\emph{satisfactory}, all parts (including sub-questions) should show reflective
insight that goes beyond % recreation of facts occurred during the project, or goes beyond 
what is stated in the material used to answer the question (e.g.
books). Motivations for each part of the answer are provided (and are reasonable
and realistic), and alternatives (where applicable) are at least mentioned. Each
question is assessed according to this ``gold standard'' as follows:

\begin{itemize}
\item[(1p)] \emph{Satisfactory}: All parts of the question are answered satisfactory.
\item[(0.7p)] \emph{Partial} Less than $\frac{2}{3}$ of the question (including sub-questions) are answered satisfactorily. The main question is answered sufficiently, but not completely.
\item[(0.4p)] \emph{Incomplete} Less than $\frac{1}{2}$ of the question (including sub-questions) are answered satisfactorily. The main question is not answered satisfactorily.
\item[(0p)] \emph{Insufficient} The question is not answered at all (blank) or the statements are not relevant for the stated question. 
\end{itemize}

Your points are then summed up, and a grade is given according to Table \ref{T:GradeLimits}.

\begin{table}[bth]
  \centering
  \begin{tabular}{rl}
    Limit & Grade\\
    \hline
    $<60\%$ & F\\
    $60-64\%$ & E\\
    $65-69\%$ & D\\
    $70-79\%$ & C\\
    $80-89\%$ & B\\
    $\geq 90\%$ & A\\
    \hline
  \end{tabular}
  \caption{Grade Limits}
  \label{T:GradeLimits}
\end{table}

\subsection{Assignment Analysis Tasks}
\begin{enumerate}
\item Study Chapter 2 (Audacity) in Volume I of ``The Architecture of Open Source Applications'' (the AOSA book), available online at \url{http://www.aosabook.org/en/audacity.html}. This is the chapter you will use to answer the questions in the central assignment tasks.
\item Work through the relevant study packages.
\item Write an assignment introduction, presenting the assignment and the system.
\end{enumerate}


\subsection{Central Assignment Tasks}
\begin{enumerate}
\item Answer the following questions based on the aforementioned chapter of ``The Architecture of Open Source Applications'':\\
\fbox{\begin{minipage}{0.9\columnwidth}
\begin{enumerate}[Q1]
\item Please list the five most important software architecture related issues. {\bf Important:} Quote the AOSA book to motivate the issues.
\item Please develop relevant, software architecture related strategies for your issues. Discuss how your strategies address the issues satisfactorily.
\item Please relate the identified strategies to tactics proposed in chapter 5 of Bass et al. (2012)\footnote{Please note that this refers to the second edition of Bass et al. In the third edition, the corresponding chapters are chapters 4 to 13}. Please explain \emph{in depth} how your strategies are concrete instantiation of the generic tactics in Bass et al.
\item Decide which issue is the most important. Discuss and motivate your anser. Quote the AOSA book where applicable.
\item Identify the view in which the strategy that best solve your selected issue is implemented, and implement the strategy. {\bf Hint:} Briefly sketch and think about all views first so that you fully understand the system, \emph{then} select which issue and strategy you find is most important, \emph{finally} decide in which view to implement your strategy.
\item Please perform an \emph{in-depth} evaluation of the architecture in the AOSA book with respect to \emph{performance}, \emph{maintainability}, and \emph{scalability}.  Quote the AOSA book for motivations of your evaluations.
\end{enumerate}
\end{minipage}}
\end{enumerate}


\subsection{Final Assignment Tasks}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}

\subsection{Deliverables}
One PDF document containing:
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Introduction}] An introduction, presenting the assignment and the
system.
 \item [{Assumptions}] A description of assumptions you have on the nature of
the application domain, the system, the environment in which it will be
deployed, the customer and users.
 \item[{Answers to Questions}] Your answers to the questions above.
\end{description}

\subsection{Relevant Study packages}
Relevant study packages include, but may not be limited to:
\begin{itemize}
\item Architecture Fundamentals
\item Architecture Documentation
\item Architecture Evaluation
\item Quality Attributes
\end{itemize}

\newpage
\section{Assignment 3: Architecture Design}
\begin{quote}
Architecture design, evaluation and transformation
\end{quote}

\noindent Based on the results from the peer evaluation and the feedback 
received on assignment 1, continue to design the architecture. Choose an
evaluation method and evaluate the architecture against relevant quality
requirements. If necessary, apply transformations and re-evaluate. Iterate
evaluation and transformation until the quality requirements are met.

For Architecture Transformations, see Bosch 2000.

\subsection{Assignment Analysis Tasks}
\begin{enumerate}
\item (individual) Work through the relevant study packages.
\item Read and analyse the system description.
\item Write an assignment introduction, presenting the assignment and the system.
\item Analyse the assignment feedback and modify the factor and issue tables from Assignment 1 accordingly. Modify your architecture to reflect the updated factors, issues, and strategies.
\item Analyse the report from the peer evaluation and transform your architecture to address the identified improvement opportunities.
\item Highlight and document changes that are made in order to facilitate marking.
\end{enumerate}

\subsection{Central Assignment Tasks}
\begin{enumerate}
\item Identify which issues and strategies that should be addressed in the module view.
\item Implement your module view using your conceptual view and the identified strategies. Document where and how each strategy is implemented.
\item Identify which issues and strategies that should be addressed in the execution view.
\item Implement your execution view using your conceptual and module view and the identified strategies. Document where and how each strategy is implemented.
\item Select an architecture evaluation method and motivate your selection.
\item Evaluate your architecture using your chosen evaluation method.
\item Transform your architecture to address the identified improvement opportunities.
\item Document your assumptions.
\end{enumerate}


\subsection{Final Assignment Tasks}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}


\subsection{Deliverables}
One PDF document containing:
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Assignment 1}] The content of assignment 1, adding and highlighting
changes that emerged from the peer evaluation and feedback.
 \item [{Architecture design}] Applying Hofmeisters' method, design the
module and the execution view. Motivate design choices by the developed
strategies!
 \item [{Evaluation method}] Provide information on the chosen
architecture evaluation method, along with a motivation why it was chosen (also
stating why other methods were rejected). \emph{Note:} This evaluation you 
perform on your own within your team, i.e. this evaluation \emph{must} be 
different from the analysis you have done in the peer evaluation seminar.
 \item [{Evaluation result}] Present the result from the architecture
evaluation.
 \item [{Transformation strategy}] If transformation is necessary, describe and
motivate the chosen transformation strategy.
 \item [{Transformation step}] Apply the transformation on the architecture,
highlighting the changes in the architecture design. It is important to
maintain a chain of evidence: issue discovered in the evaluation $\rightarrow$
chosen transformation $\rightarrow$ impact on the architecture and architecture decisions.
If strategies are added/modified, document the changes.
 \item [{Re-evaluate}] In order to see whether the transformation had the
desired effect on the architecture, you need to perform a second evaluation.
Document the results and re-iterate (evaluate, choose transformation strategy,
transform, re-evaluate) if the architecture still does not meet the quality
requirements.
\end{description}

\subsection{Relevant study packages}
Relevant study packages include, but may not be limited to:
\begin{itemize}
 \item Architecture Evaluation 
 \item Architecture Transformations
\end{itemize}

\newpage
\section{Assignment 4: Formal Specifications}
\begin{quote}
Formal architecture specification and evaluation
\end{quote}

\noindent In this assignment you will extend an existing architecture model 
represented in AADL and perform resource allocation and latency evaluations.

The system description, the architecture model and a more detailed
description of this assignment are provided in a separate document and are
available on the course homepage.

\subsection{Assignment Analysis Tasks}
\begin{enumerate}
\item (individual) Work through the relevant study packages.
\item Read and analyse the system description.
\item Write an assignment introduction, presenting the assignment and the system.
\end{enumerate}

\subsection{Central Assignment Tasks}
\begin{enumerate}
\item Work through the assignment as described in the separate document.
\end{enumerate}


\subsection{Final Assignment Tasks}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}

\subsection{Relevant study packages}
Relevant study packages include, but may not be limited to:
\begin{itemize}
 \item AADL
 \item Architecture Evaluation
\end{itemize}


\newpage
\section{Assignment 5: Architecture Change}

\begin{quote}
Changeability, anyone?
\end{quote}

\noindent In this assignment we introduce changes to the initial system 
description. You need to analyze whether your architecture
design still fulfills the requirements and trace any and all changes.

\subsection{Assignment Analysis Tasks}
\begin{enumerate}
\item (individual) Work through the relevant study packages.
\item Read and analyse the system description.
\item Write an assignment introduction, presenting the assignment and the system.
\item Analyse the assignment feedback and modify the factor and issue tables from Assignment 3 accordingly. Modify your architecture to reflect the updated factors, issues, and strategies.
\item Highlight and document changes that are made in order to facilitate marking.
\end{enumerate}

\subsection{Central Assignment Tasks}
\begin{enumerate}
\item Analyse the change requests to understand their implications on the system and on your architecture.
\item Identify factors, issues, and strategies that need to change in order to accomodate the change requests.
\item Update factors, issues, and strategies.
\item Update your architecture views (conceptual, module, execution views) according to the changed factors, issues, and strategies. Highlight and document your changes in order to facilitate marking.
\item Choose a method to evaluate the architecture.
\item Evaluate the architecture to ensure that the quality requirements are still met.
\item (if necessary) Transform the architecture to address the identified improvement opportunities.
\item Document your assumptions.
\end{enumerate}


\subsection{Final Assignment Tasks}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}


\subsection{Deliverables}
One PDF document containing:
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Assignment 3}] The content of assignment 3, adding and highlighting
changes that emerged from the feedback
 \item [{Change requests}] Motivate your choice of change requests and why
you don't address the remaining ones. 
 \item [{Impact analysis}] Analyze the impact of the system changes on factors,
issue cards and developed strategies. It is important that you show how a change
propagates from factors to issues cards to strategies to architecture views. 
Answer the following questions:
 \begin{enumerate}
  \item Which factors have changed? How?
  \item If factors have changed, which areas in the architecture are affected?
  \item Is it necessary to introduce new factors? Which?
  \item Do the existing issue cards, and the corresponding strategies, consider
and cope with the changes? If not, how shall they be changed?
 \end{enumerate}
 \item[{Implemented changes}] The updates to your architecture.
 \item [{Evaluation method}] Motivate why you either re-apply the evaluation
method from assignment 2 or choose a different method to assess whether your
architecture fulfills the new/modified quality requirements.
 \item [{Evaluation result}] Present the result from the architecture
evaluation.
 \item [{Transformation strategy}] If transformation is necessary, describe and
motivate the chosen transformation strategy.
 \item [{Transformation step}] Apply the transformation on the architecture,
highlighting the changes in the architecture design. It is important to
maintain a chain of evidence: issue discovered in the evaluation $\rightarrow$
chosen transformation $\rightarrow$ impact on the architecture. If strategies
are added/modified, document the changes.
 \item [{Re-evaluate}] In order to see whether the transformation had the
desired effect on the architecture, you need to perform a second evaluation.
Document the results and re-iterate (evaluate, choose transformation strategy,
transform, re-evaluate) if the architecture does not meet the quality
requirements.
\end{description}

\subsection{Relevant study packages}
Relevant study packages include, but may not be limited to:
\begin{itemize}
 \item Architecture Evaluation 
 \item Architecture Transformations
\end{itemize}

\end{document}
