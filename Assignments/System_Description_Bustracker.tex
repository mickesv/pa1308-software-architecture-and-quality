\documentclass{article}
\usepackage{fullpage}
\usepackage{times}
\title{BusTracker}
\author{Courtesy of Ewan Tempero\\
Computer Science\\
University of Auckland\\
Private Bag 92019\\
Auckland 1\\
New Zealand\\
}
\date{}
\begin{document}
\maketitle

\section{Introduction}

This provides some details of the requirements for a system that you will be
developing a software architecture for. The real requirements for this kind
of system would fill a very large book, so what is here is necessarily
incomplete. There is enough detail to provide a realistic situation for the
assignment, but in the case where you need more details, feel free to make
some up. If you do, {\em state them clearly} so that the markers know what
assumptions you are making.

\section{Overview}

BusTracker is a system that has been requested for the Auckland region by
the Auckland Regional Council.\footnote{This is untrue only in that I have
had no contact with ARC and so all the details I provide here are completely
made up by me. ARC is, however, in the process of developing such a system.}
The initial brief for this system is given below.

\begin{quote}
\em
ARC wants a system called BusTracker that tracks buses.  It
wants to add GPS to all of its buses so that it can track where they are to
within 100 metres. They will use this information to provide estimated
arrival times of buses at each major bus stop.

The unit to be placed on each bus consists of a Global Positioning System
(GPS) receiver, a radio transmitter, and other bits of hardware and
software. The GPS receiver can can determine its position to within 10
metres at each second. If calibrated properly, it can reliably track the bus
position and speed throughout the journey. It transmits this information,
along with the bus' identifier to the BusTracker system on a regular basis.

The major bus stops are where a number of bus routes converge. There are
typically 20 or more buses that stop at these stops during peak travel
times. The planned displays will have a radio receiver and room for display
four or five bus numbers and times (that is, similar to those that already
exist for the LINK bus). 

The displays should repeatedly scroll through all the buses whose estimated
or scheduled arrival times at that stop are sometime in the next hour. Once
the bus is within 1 kilometre (that is, about 2 bus stops away) of a
display, the estimated arrival time should be within 2 minutes of the actual
time, 95\% the time. 
All other displays should show a ``best effort'' estimated time.

The bus company also would like to allow bus users to get estimated arrival
times for all buses at all times via their web site, and also via phone.
\end{quote}

\section{System Architecture}

Some aspects of the system are constrained as follows:

\begin{itemize}
  \item
    There is a single radio receiver for the bus transmissions for the
    city. It's located in place that covers all possible places a bus
    will transmit from.
  \item
    ARC has a number of buildings where the computer hardware can be located
    and can receive the information from the radio receiver via the
    Internet.
  \item
    The hardware on a bus can transmit a message containing
    the unique bus ID, timestamp, and location every second. The 
    timestamp and location comes from the GPS receiver on request.
  \item
    Historical data for trip times is kept for 24 months.
  \item
    The displays have a unique id.  They receive information from the
    central system, not from the buses. There is no communication between
    bus and display in either direction. There is also no communication
    from display to central system. The displays just receiving information
    from the central system and show it.

    The displays should be updated not more than once a minute (more
    details below).

    
\end{itemize}

\section{Functional Requirements Detail}

Here are the details of the functionality you are to concentrate on.
The functionality is presented as use cases in a particular 
format.

\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf Show next buses} (Display)  } \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{-- Specify location\\[1mm]} & \\ \hline
& \parbox{7cm}{-- Verify information source\\
-- Show the current time\\
-- List the bus number, bus route name, scheduled arrival time, and estimated arrival time for all buses expected or scheduled to arrive at the specified location in the hour following the time of the request.

The estimated
time must be within 2 minutes of the actual arrival time if the bus
is within 1 kilometre of the display, otherwise it should be ``best
effort''.
  \\[1mm]} \\ \hline
\multicolumn{2}{||p{14cm}||}{\em 
If there is information to display, the information must be provided within 15 seconds of the request.

\smallskip
\noindent {\bf Notes}
\begin{itemize}
\vspace*{-2mm}
\itemsep -1mm
\item
``location'' here means location of the display. Note that there are
many ways to specify the display's location. It could, for example, be
done by specifying the display's ID.

\item
If a bus is scheduled to arrive before the time of the request, but is
expected to arrive after the time of the request (i.e., it's late), it
should still be listed because it is expected arrival time is within an
hour.

\item
If a bus is scheduled to arrive within 50 minutes of the time of the
request, but is running 15 minutes late (so the estimated arrival time
is over an hour away), it should still be listed because it's scheduled
to arrive within an hour.
\end{itemize}
} \\ \hline \hline 
\end{tabular}

\medskip
\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf Indicate system problem EXTENDS Show next buses} (Display)  } \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{} & \\ \hline
& \parbox{7cm}{-- Indicate that information is not available\\[1mm]} \\ \hline
\multicolumn{2}{||p{14cm}||}{\em Rationale: The display must make it clear when passengers cannot rely on the display.
} \\ \hline \hline 
\end{tabular}

\medskip
\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf Show estimated arrival time for bus} } \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{-- Specify bus and bus stop\\[1mm]} & \\ \hline
& \parbox{7cm}{-- Display the bus route number, bus route name, scheduled and estimated times that the specified bus will arrive at the specified bus stop\\[1mm]} \\ \hline
\multicolumn{2}{||p{14cm}||}{\em 

\noindent{\bf Notes}
\begin{itemize}
\item
A bus is defined by the bus route and the scheduled time a bus is supposed
to start on that route.

\item
Bus stop is any valid bus stop (not just ``major'' bus stops)
\end{itemize}
} \\ \hline \hline 
\end{tabular}

\medskip
\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf Show estimated arrival time for next bus} } \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{-- Specify bus route and bus stop\\[1mm]} & \\ \hline
& \parbox{7cm}{-- Display the bus route number, bus  route name, scheduled and estimated  times for the next bus on the specified bus route predicted to arrive at the specified bus stop after the time of the request\\[1mm]} \\ \hline
\end{tabular}

\medskip
\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf List buses for route at bus stop} } \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{-- Specify bus route and bus stop\\[1mm]} & \\ \hline
& \parbox{7cm}{-- List all buses on the specified bus route and their scheduled arrival times at the specified bus stop for today\\[1mm]} \\ \hline
\end{tabular}

\medskip
\begin{tabular}{||p{7cm}|p{7cm}||} \hline\hline
\multicolumn{2}{||p{14cm}||}{{\bf List bus routes for bus stop}} \\ \hline
{\bf User intention} & {\bf System Responsibility} \\ \hline
\parbox{7cm}{-- Specify bus stop\\[1mm]} & \\ \hline
& \parbox{7cm}{-- List all bus routes that stop at the specified bus stop\\[1mm]} \\ \hline
\end{tabular}

\section{Details and Issues}

Here are some issues that you may need to consider.

\begin{itemize}
\item
There are several ``non functional'' requirements to think about (or
consequences of non functional requirements).
\begin{itemize}
\item
The main one is performance. The {\bf Show next buses} use case has a stated
performance requirement. In order to meet it, you need to get information to
the display in a timely manner (discussed further below). In order to do
that, you need to do the estimation in a timely manner, which has processor
hardware implications.  In order to to that, you need the inputs to the
estimation algorithm, which has storage implications. The inputs to the
estimation algorithm include (at least) the current bus positions and
speeds, so there are communication bandwidth and storage implications.
This may not be a complete list.

\item
The web site and phone access was mentioned mainly to give an indication
that there may be future development. This has implications about some
of the quality attributes (e.g., modifiability, reusability).

\item
It was never stated anywhere, but there is almost certainly an expectation
that this system will last a while (decades rather than years). This
has further implications for the quality attributes
(e.g., portability).
\end{itemize}


\item
If the information a display receives takes more than 1 minute to scroll
through, then it will request less than once a minute. If it takes less than
one minute, it will just redisplay what it has rather than updating it.

(I'm assuming there will never be so much information to display that this
will cause a problem.  Assume there are 40 buses of information to
display. The display can show 5 of them at a time. If it scrolls the top one
off and the bottom one on every second, it will take 45 seconds for them all
to be shown for at least 5 seconds (1 second on each row).)

\item
You may assume 100 major bus stops, and 1000 buses on the road at any time.

\item
The phrase ``on a regular basis'' is meant to be deliberately vague.
In the first paragraph, there is a mention of being able to locate
the bus within 100 metres. If the bus is stopped (bus stop or traffic
light), then it may not need to transmit every second. But if the
bus is travelling 100km/h (whether or not this is legal!), it can
do about 30metres in 1 second, so maybe it needs to transmit
more frequently than when it is stopped. You have to decide what
to do about this. (Note that the frequency this information is
transmitted affects the amount of information the system has
to handle, which has implications for bandwidth, storage, and
processor speed).
\end{itemize}


\section{Notes}

Here are some things to keep in mind.

\begin{itemize}
\item
You are supposed to be coming up with the {\em software} architecture for
Bus Tracker. You will need to think about the hardware, but don't spent too
long on it --- make some reasonable decisions and design your software
around it. (A not-unrealistic situation - you're called in to design
software for hardware that's already purchased\ldots).

\item
There are going to be some things you won't know that you need to know to
make some of your arguments. For example, what processing power of different
kinds of processors have, or how much memory costs. In such cases, make a
reasonable assumption and {\em state it}. (Avoid making assumptions that
make your architecture trivial!)

\item
The goal of this description is to give an introduction and a context to the system. If you find the need to add requirements or to make assumptions, please feel free to do so. Some requirements in this description are vaguely formulated on purpose - you are expected to clarify them. Note that you have to document your assumptions in order to make the reader of the report aware of the changes made. 

\end{itemize}

\end{document}
