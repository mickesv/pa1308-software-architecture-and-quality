\documentclass[10pt]{beamer}

%% Use this for 4 on 1 handouts
%\documentclass[handout]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[landscape, a4paper, border shrink=5mm]

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\def\subitem{\item[\hspace{1.5cm} -]}

\usepackage{graphvizzz}

% Set the presentation mode to BTH
\mode<presentation>
{
	\usetheme{BTH_msv}
	% Comment this if you do not want to reveal the bullets before they are going to be used
	\setbeamercovered{transparent}
}


% Information for the title page

\title[]{Global Analysis}
\subtitle{PA1410 Software Architecture and Quality}
\date[]{}

\author[Mikael Svahnberg]{Mikael Svahnberg\inst{1}}
\institute[BTH] % (optional, but mostly needed)
{
  \inst{1}%
 Mikael.Svahnberg@bth.se\\
 School of Computing\\
 Blekinge Institute of Technology%
}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSection[]
{
 \begin{frame}<beamer>{Outline}
   \tableofcontents[currentsection]
 \end{frame}
}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 
%\beamerdefaultoverlayspecification{<+->}

\begin{document}

% Titlepage frame
\begin{frame}
  \titlepage
\end{frame}

% ToC frame
% Use \section and \subsection commands to get things into the ToC.
%\begin{frame}
 %\frametitle{Outline}
 % \tableofcontents
%\end{frame}

% -----------------------------
% Your frames goes here
% -----------------------------

\section{Background}

\begin{frame}[t]
\frametitle{Background on Global Analysis}

\begin{itemize}
\item Hofmeister et al. (2000) ``Applied Software Architecture''.
\item A structured way of analysing a problem to find architecture drivers
\item Overall development cycle:
\end{itemize}

\digraph[height=6cm]{FHofmeisterProcess}{
  GA [label="Global Analysis"];
  subgraph cluster_View {
    label="Conceptual/Module/Code/Execution View";
    ViewCDT [label="Central Design Tasks"];
    ViewFDT [label="Final Design Tasks"];
    GA -> ViewCDT;
    ViewCDT -> ViewFDT;
  };
}
\end{frame}

\begin{frame}[t]
\frametitle{Global Analysis Overview}

\digraph[width=\columnwidth]{FGlobalAnalysisOverview}{
  rankdir=LR;
  node [shape=box,style=filled,fillcolor=white];
  subgraph {
    OF [label="Organisational Factors"];
    TF [label="Technological Factors"];
    PF [label="Product Factors"];
  };
  subgraph cluster_View {
    label="An Architecture View"; shape=box; style=filled; fillcolor=lightgray;
    subgraph cluster_GA {
      style="rounded,filled";label="Global Analysis";fillcolor=white;
      AF [label="Analyse Factors"];
      DS [label="Develop Strategies"];
      AF -> DS [label="Factor Tables"];
      /*DS -> AF [label="new factors"];    */
    };    
    CDT [label="Central Design Tasks"];
    FDT [label="Final Design Tasks"];
    DS->CDT [label="Issue Cards"];
    /*CDT->DS [label="new issues or strategies"];
    CDT->AF [label="new factors"];*/
    CDT->FDT;
  };
  OF -> AF;
  TF -> AF;
  PF -> AF;
}

\end{frame}

\begin{frame}[label=SAnalyseFactors,t]
\frametitle{Analyse Factors}
\only<1>{
Different types:
\begin{itemize}
\item Organisational Factors, e.g. development schedule, budget, in-house skills, development process
\item Technological Factors, e.g. hardware, third party software, standards, architecture technology
\item Product Factors, e.g. functional features, quality requirements
\end{itemize}
}

Steps
\begin{enumerate}
\item Identify and Describe the Factors
\item Characterise the Flexibility and Changeability of the Factors
\item Analyse the Impact of the Factors
\end{enumerate}

\only<2>{\begin{block}{1. Identify and Describe the Factors}
Does the factor have global influence (i.e., does it need to be dealt with by the [software] architecture)?
\begin{itemize}
\item Can the factor's influence be localised to one component in the design?
\item During which stages of development is the factor important?
\item Does the factor require any new expertise or skill?
\end{itemize}
\end{block}}

\only<3>{\begin{block}{2. Characterise the Flexibility and Changeability of the Factor}
What can be negotiated (flexibility), what may change (changeability)
\begin{scriptsize}
\begin{itemize}
\item[F] Is it possible to influence or change the factor to make your task of architecture development easier?
\item[F] In what way can you influence it?
\item[F] To what extent can you influence it?
\item[C] In what way could the factor change?
\item[C] How likely will it change during or after development?
\item[C] How often will it change?
\item[C] Will the factor be affected by changes in other factors?
\end{itemize}
\end{scriptsize}
\end{block}}

\only<4>{\begin{block}{3. Analyse the Impact of the Factors}
If the factor was to change, which of the following would be affected, and how:
\begin{itemize}
\item Other factors
\item Components
\item Modes of operation of the system
\item Other design decisions
\end{itemize}
\end{block}}

\end{frame}


\begin{frame}[label=SDevelopStrategies,t]
\frametitle{Develop Strategies}
\begin{enumerate}
\item Identify Issues and Influencing Factors
\item Develop Solutions and Specific Strategies
\item Identify Related Strategies
\end{enumerate}

\only<2>{\begin{block}{Step 1. Identify Issues and Influencing Factors}
\begin{scriptsize}
An Issue is something that needs to be addressed in the [software] architecture, often stemming from several factors and their changeability.
\begin{itemize}
\item An issue may arise from limitations or constraints imposed by factors. For example, agressive development schedule vs requirements overload.
\item An issue may result from the need to reduce the impact of changeability of factors. For example, design the architecture to reduce the cost to porting to other operating systems.
\item An issue may develop because of difficulties in satisfying product factors. For example, high throughput requirements may overload the CPU.
\item An issue may arise from the need to have a common solution to global requirements. For example, error handling and recovery.
\end{itemize}
\end{scriptsize}
\end{block}}

\only<3>{\begin{block}{Step 2. Develop Solutions and Specific Strategies}
\begin{scriptsize}
How you intend to address the issue. Design each strategy to be consistent with
\begin{itemize}
\item influencing factors
\item desired/required changeability
\item interactions with other factors
\end{itemize}

Address the following goals:
\begin{itemize}
\item Reduce or localise the factor's influence.
\item Reduce the impact of the factor's changeability on the design and other factors.
\item Reduce or localise required areas of expertise or skills.
\item Reduce overall time and effort.
\end{itemize}
\end{scriptsize}
\end{block}}

\end{frame}

\section{Example}

\begin{frame}[t]
\frametitle{Example: Robot}
{\it Design an architecture for the application domain of mobile robots, for example the robot used in the latest mars expedition by NASA, a cleaning robot for sewerage pipes, oil pipeline maintenance robots or even a bomb disarming robot.}

\begin{itemize}
\item Embedded Real-time system
\item External Sensors and Actuators
\begin{itemize}
\item Acquire input from sensors
\item control motion of wheels and moveable parts (actuators)
\item Plan the future path
\end{itemize}
\item Factors that complicate:
\begin{itemize}
\item Obstacles
\item Imperfect input
\item Power shortage
\item Mechanical limitations reduce accuracy of actuator movements
\item May manipulate hazardous materials
\item Unpredictable events may leave little time for responding
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Design Considerations}
The architecture must accomodate:
\begin{itemize}
\item deliberate as well as reactive behaviour
\item uncertainty; re-planning and reacting
\item dangers inherent in the robot's operation and environment (fault tolerance, safety, performance)
\item flexibility for the designer
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Global Analysis / Analyse Factors}
\begin{itemize}
\item Organisational factors
\only<2>{\subitem Leave these for now}
\item Technical factors
\only<3>{
\subitem General-Purpose Hardware: Processors, Network, Memory, Disk
\subitem Domain-Specific Hardware: Sensor hardware, actuator hardware
\subitem Software Technology: Operating System, User Interface, Software Components, Implementation Language, Design Patterns, Frameworks
\subitem Architecture Technology: Architecture styles, patterns, frameworks, domain specific architectures, architecture description languages, product-line-technologies
\subitem Standards
}
\item Product factors
\only<4>{
\subitem Functional Features
\subitem User Interface
\subitem Performance, Dependability (Availability, Reliability, Safety)
\subitem Failure detection, reporting, recovery: Error classification, Error logging, diagnostics, recovery
\subitem Service: Service features, software installation and upgrade, maintenance of hardware, software testing, software maintenance
\subitem Product Cost: hardware budget, software licencing budget
}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Analyse Factors (examples)}
\begin{scriptsize}{\bf Note:} Examples are not structured according to organisational, technological, or product factors.\end{scriptsize}
\begin{tabular}{*{3}{p{3.5cm}}}
Factor & Flexibility \& Changeability & Impact\\
\hline
\only<1->{Networked User Interface} & \only<2->{C: May extend to radio-controlled} & \only<3->{New sensor; new UI component} \\
 \only<4->{User-defined set of Actuators} & \only<5->{F: Ok to select from pre-defined} & \only<6->{No need for open API for actuators / no need to sandbox actuator controllers} \\
\only<7->{Sensor Interpretation is based on multiple sensors} & \only<8->{None} & \only<9->{-} \\
% \only<>{} & \only<>{} & \only<>{} \\
% \only<>{} & \only<>{} & \only<>{} \\
% \only<>{} & \only<>{} & \only<>{} \\
% \only<>{} & \only<>{} & \only<>{} \\
\hline
\end{tabular}
\end{frame}

\againframe[t]{SDevelopStrategies}

\begin{frame}[t]
\frametitle{Develop Strategies (examples)}
Issues
\begin{itemize}
\item Mechanical limitations in actuators: accuracy of actuator movements are less than optimal.
\subitem Solution: Use sensors to continuously verify actuator movements
\item Rough Environment: Communication with driver may not always work
\subitem Solution: Build plan based on driver's previous input
\subitem Solution: Buffer feedback for driver and re-send until acknowledged
\subitem Solution: Verify sanity of input against plan and previous input
\item Unknown number of sensors/actuators may slow down main control loop
\subitem Solution: Parallelize sensor/actuator processing
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Conceptual View}
\begin{scriptsize}{\bf Note:} Creating viewpoints is not the goal of this lecture; this is included as a reference to understand how to go from issues and strategies to a design in a traceable way.
\end{scriptsize}

\begin{itemize}
\item Which Issues are useful to create this view?
\only<2-3>{
\subitem Mechanical limitations in actuators: requires synchronisation between actuators and sensors $\rightarrow$ interaction between components
\subitem Rough Environment: new components (PlanCreator, FeedbackBuffer, InputSanityChecker)
\subitem \emph{Not} Unknown number of sensors and actuators
}
\only<3>{\begin{block}{Why not?}
Parallelization does not \emph{per se} create any new components; it simply allocates the components to threads, processes, and processors. You may create a new coordination component but we will have this anyway in order to interpret sensor input based on multiple sensors.
\end{block}}
\item Which Factors?
\only<4->{
\subitem Networked User Interface: new components (UserIO, UserInterface), interactions with components
\subitem Sensor Interpretation is based on multiple sensors: new components (SensorInputInterpreter)
\subitem \emph{Not} User-defined set of Actuators
}
\only<5>{\begin{block}{Why not?}
Does not create any new components, it is just a configuration item that can be dealt with by any number of variability realisation techniques\footnote{M. Svahnberg, J. Van Gurp, and J. Bosch. A taxonomy of variability realization techniques. in \emph{Software- Practice and Experience}, 35(8):705--754, 2005.}
\end{block}}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Conceptual View}
\begin{itemize}
\item Identifying the issues and factors like this provides a rudimentary traceability.
\item Next, you start designing. Start with a large box and add components as you need to provide the functionality of the system.
\item Take care to address any factors you had regarding architectural styles, domain specific architectures, etc.
\subitem For example, Alberto Elfes\footnote{Elfes. Sonar-Based Real-World Mapping and Navigation. IEEE Journal of Robotics and Automa- tion, no.3, 1987, pp. 249-265.} provides a layered architecture for robots; These layers will probably be represented by (sets of) components in some way in the conceptual view, and even more so in the module view.
\end{itemize}

\end{frame}

\begin{frame}[t]
\frametitle{Conceptual View}

\digraph[width=\columnwidth]{FRobotConceptualExample}{
rankdir=LR;
node [shape=box];
edge [dir=both,arrowhead=box,arrowtail=box];
subgraph cluster_Robot {
  label=":Robot";
  UserIO->UserInterface;
  PlanCreator->PlanManager;
  UserIO->PlanCreator;
  UserIO->FeedbackBuffer;
  UserInterface->PlanManager;
  PlanManager->InputSanityChecker;
  PlanCreator->InputSanityChecker;
  PlanManager->SensorInputInterpreter;
  SensorInputInterpreter->Sensor;
  PlanManager->ActuatorManager;
  ActuatorManager->Actuator;
  ActuatorSensorInterpreter->SensorInputInterpreter;
  ActuatorSensorInterpreter->ActuatorManager;
  SensorInputInterpreter->Navigation;
  Navigation->PlanManager;
  PlanManager->Storage;
  subgraph logic {
    rank=same;
    PlanManager; Navigation; Storage; InputSanityChecker;
  }
}
}

\end{frame}


\begin{frame}[t]
\frametitle{Summary}
\begin{itemize}
\item Global Analysis is a \emph{tool} to help you identify architecture relevant concerns
\item It provides a \emph{structured approach}
\item It provides basic \emph{traceability} from your concerns to your actual architecture
\item It does not help you to actually create a workable architecture; it just makes sure you do not forget anything.
\end{itemize}
\end{frame}




% -----------------------------


%% All of the following is optional and typically not needed. 
%\appendix
%\begin{frame}[allowframebreaks]
%  \frametitle{For Further Reading}
%    
%  \begin{thebibliography}{10}
%    
%  \beamertemplatebookbibitems
%  % Start with overview books.

%  \bibitem{Author1990}
%    A.~Author.
%    \newblock {\em Handbook of Everything}.
%    \newblock Some Press, 1990.
%     
%  \beamertemplatearticlebibitems
%  % Followed by interesting articles. Keep the list short. 

%  \bibitem{Someone2000}
%    S.~Someone.
%    \newblock On this and that.
%    \newblock {\em Journal of This and That}, 2(1):50--100,
%    2000.
%  \end{thebibliography}
%\end{frame}

\end{document}


