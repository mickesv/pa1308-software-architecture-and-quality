\documentclass[10pt]{beamer}

%% Use this for 4 on 1 handouts
%\documentclass[handout]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[landscape, a4paper, border shrink=5mm]

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphvizzz}
\def\subitem{\item[\hspace{1.5cm} -]}

% Set the presentation mode to BTH
\mode<presentation>
{
	\usetheme{BTH_msv}
	% Comment this if you do not want to reveal the bullets before they are going to be used
	\setbeamercovered{transparent}
}


% Information for the title page

\title[]{Architecture Evaluations\\and Transformations}
\subtitle{L05 PA1308}
%\date[]{}

\author[Mikael Svahnberg]{Mikael Svahnberg\inst{1}}
\institute[BTH] % (optional, but mostly needed)
{
  \inst{1}%
 Mikael.Svahnberg@bth.se\\
 School of Computing\\
 Blekinge Institute of Technology%
}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%\AtBeginSubsection[]
%{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
%}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 
%\beamerdefaultoverlayspecification{<+->}

\begin{document}

% Titlepage frame
\begin{frame}
  \titlepage
\end{frame}

% ToC frame
% Use \section and \subsection commands to get things into the ToC.
%\begin{frame}
 %\frametitle{Outline}
 % \tableofcontents
%\end{frame}

% -----------------------------
% Your frames goes here
% -----------------------------

\begin{frame}[t]
\frametitle{Purpose of Architecture Evaluations}

\begin{itemize}
\item Early Architecture Evaluation\footnote{M. Lindvall, R. T. Tvedt, and P. Costa. An empirically-based process for software architecture evaluation. \emph{Empirical Software Engineering}, 8:83--108, 2003.}
\onslide<2->{
\subitem Do we meet the quality requirements on the system?
\subitem Do all stakeholders share a common understanding of the system?
\subitem Are all requirements accounted for?
\subitem Are there any weak spots in ther architecture?
\subitem Can the system (and/or the architecture) be improved?
\subitem Does the development team have all the necessary resources?
\subitem Should we let this project continue?
}
\item Late Architecture Evaluation
\onslide<3->{
\subitem Hard metrics.
\subitem How did we do? What needs to be improved for the next release?
}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Early Architecture Evaluation Methods}
\begin{itemize}
\item Experiences and Logical Reasoning
\item Scenario-Based
\subitem<2-> Examples: SAAM, ATAM, Global Analysis, BTH-way
\item Simulation-based
\subitem<3-> Build parts of the architecture / Build models of the architecture
\subitem<3-> Architecture description languages
\subitem<3-> Examples: AADL, Aesop, ArTek, C2, Darwin, LILEANNA, MetaH, Rapide, SADL, UniCon, Weaves, Wright, ACME, \ldots
\item Based on Mathematical models
\subitem<4-> Often domain-specific
\subitem<4-> Example: ABAS (ATAM)
\item Other
\subitem<5-> Example: Software Inspections
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{SAAM: Software Architecture Analysis Method I}
\begin{enumerate}
\item Develop Scenarios\footnote{R. Kazman, L. Bass, M. Webb, and G. Abowd. Saam: A method for analyzing the properties of software architectures. In \emph{Proceedings of the 16th international conference on Software engineering}, pages 81--90. IEEE Computer Society Press, 1994.}
\item Describe Candidate Architecture
\item Classify Scenarios
\subitem Directly supported vs. Indirectly supported
\item Perform Scenario Evaluations for indirect scenarios
\subitem Needed changes to support scenario, cost of changes.
\subitem C.f. Architecture Transformations
\item Reveal Scenario Interaction
\subitem Scenarios requiring changes in the same components $\rightarrow$ component overload?
\item Overall Evaluation
\subitem Prioritise scenarios
\end{enumerate}
\end{frame}

\begin{frame}[t]
\frametitle{SAAM: Software Architecture Analysis Method II}
\digraph[height=7cm]{FSAAM}{
	subgraph a1{
	ss1 [label="Scenario Development"];
	ss2 [label="Architecture Description"];
	}
	ss3 [label="Classify Scenarios"];
	ss4 [label="Evaluate Indirect Scenarios"];
	ss5 [label="Scenario Interaction"];	
	ss6 [label="Overall Evaluation"];
	ss1->ss2; ss2->ss1;
	ss1->ss3; ss2->ss3;
	ss1->ss4; ss2->ss4; ss3->ss4;
	ss4->ss5;
	ss5->ss6; ss4->ss6;
}
\end{frame}

\begin{frame}[t]
\frametitle{ATAM: Attribute Tradeoff Analysis Method}
\begin{scriptsize}
\begin{itemize}
\item Based on SAAM
\item Focus on quality attributes
\item Participants:
\subitem Evaluation Team\onslide<2->{: Team Leader, Evaluation Leader, Scenario Scribe, Proceedings Scribe, Timekeeper, Process Observer, Process Enforcer, Questioner}
\subitem Project Decision Makers\onslide<2->{: Project manager, Customer?, Architect, Commissioner of Evaluation}
\subitem Architecture Stakeholders\onslide<2->{: Developers, Testers, Integrators, Maintainers, Performance Engineers, Users, Builders of Related Systems, etc.}
\item Outputs:
\subitem A concise presentation of the architecture
\subitem Articulation of business goals
\subitem Quality requirements, expressed as scenarios
\subitem Mapping Architectural decisions to quality requirements
\subitem Set of identified sensitivity and tradeoff points
\subitem Set of risks and nonrisks
\subitem Set of risk themes
\end{itemize}
\end{scriptsize}
\end{frame}

\begin{frame}[t]
\frametitle{Phases of ATAM}
\begin{scriptsize}
\begin{itemize}
\item Phase 0: Partnership and preparation (Duration: Informally, a couple of weeks)
\item Phase 1: Evaluation (Evaluation team, project decision makers) (Duration: 1 day plus hiatus of 2-3 weeks)
\subitem<2-> Step 1: Present ATAM
\subitem<2-> Step 2: Present Business Drivers
\subitem<2-> Step 3: Present Architecture
\subitem<2-> Step 4: Identify Architectural Approaches
\subitem<2-> Step 5: Generate Quality Attribute Utility Tree
\subitem<2-> Step 6: Analyse Architectural Approaches
\item Phase 2: Evaluation (Continued) (Evaluation team, project decision makers, stakeholders) (Duration: 2 days)
\subitem<2-> Step 7: Brainstorm and Prioritise scenarios 
\subitem<2-> Step 8: Analyse Architectural Approaches
\subitem<2-> Step 9: Present Results 
\item Phase 3: Follow-Up (Evaluation team, evaluation client) (Duration: 1 week)
\end{itemize}
\end{scriptsize}
\end{frame}

\begin{frame}[t]
\frametitle{CBAM: Cost Benefit Analysis Method}
\begin{itemize}
\item Takes off where ATAM ends.
\item What is the \emph{utility} $=(\frac{B_i}{C_i})$ of supporting a quality attribute better?
\end{itemize}
\digraph[width=10cm]{FCBAM}{
	rankdir=LR;
	ss1 [label="Business Goals", shape="rectangle"];
	ss2 [label="Architecture Strategies", shape="rectangle"];
	ss3 [label="Cost", shape="diamond"];
	ss4 [label="Performance"];
	ss5 [label="Security"];
	ss6 [label="Modifiability"];
	ss7 [label="Usability"];
	ss8 [label="..."];
	ss9 [label="Benefit", shape="diamond"];
	ss1->ss2;
	ss2->ss3;
	ss2->{ss4,ss5,ss6;ss7,ss8}->ss9;
}
\end{frame}

\begin{frame}[t]
\frametitle{QASAR}
\begin{footnotesize}
J. Bosch. \emph{Design \& Use of Software Architectures - Adopting and Evolving a Product Line Approach}. Addison-Wesley, Harlow UK, 2000.
\end{footnotesize}

\digraph[height=6cm]{FQASAR}{
	ss1 [label="Functionality-Based Design", shape="rectangle"];
	ss2 [label="Software Architecture"];
	ss3 [label="Architecture Assessment", shape="rectangle"];
	ss4 [label="Architecture Transformation", shape="rectangle"];
	ss1->ss2->ss3->ss4->ss2;
}
\end{frame}

\begin{frame}[t]
\frametitle{BTH 4-hour Architecture Evaluation}
\begin{itemize}
\item Shorter variant of SAAM/ATAM.
\item Used primarily for student projects.
\item Have been used with industry partners as well.
\item Will be used by you in this course.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{BTH 4-hour Architecture Evaluation}
\begin{tabular}{lll}
Step & Name & Time\\
\hline
1 & Introduction & 10 min\\ 
2 & Identify Quality Requirements & 30 min\\
3 & Elicit Scenarios & 50 min\\
4 & Architecture presentation & 2 x 15 min\\
5 & Break & 20 min\\
6 & Scenario and Architecture Analysis & 2 x 40 min\\
7 & Conclusion & 15 min\\
\hline
\end{tabular}
\end{frame}

\begin{frame}[t]
\frametitle{Evaluation Experiences}
\begin{itemize}
\item Size of Development Team\footnote{M. Svahnberg and F. Mårtensson. Six years of evaluating software architectures in student projects. \emph{The Journal of Systems \& Software}, 80(11):1893--1901, 2007.}
\item Clear Objective
\item Present Recipients of Evaluation Document
\item Moderate Pursuit of Issues
\item Use Extreme Scenarios
\item The Impact of the Project Manager
\item Summarise Often
\onslide<2->{
\item Guidance -- not Lecturing
\item Avoid Grading Tension
\item Make the Architecture Matter
\item Encourage Peer Evaluation
}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Size of Evaluation Team}
\begin{itemize}
\item 3-4 persons in the Evaluation Team
\subitem Fewer is harder
\subitem More may intimidate the evaluatees
\item Task division:
\subitem One person documents
\subitem The others take turn in thinking / pursuiting issues
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Clear Objective of Evaluation\\Present Recipients of Evaluation}
\begin{itemize}
\item Open target = no end criterion
\item Need clear objective to decide on most appropriate method and most appropriate participants
\item Avoid objective guessing
\subitem For example, \emph{``You are here to fail us and stop the project!''}
\item Make sure there are clear benefits for the project
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Moderate Pursuit of Issues}
\begin{itemize}
\item Conflict: You are there to find flaws, but if you do not know when to ``let go'' the project will become defensive and uncooperative.
\item Knowing when to back down is not only a technical skill.
\item Difficult to identifu and investigate all ripple effects.
\item $\rightarrow$ Leave warnings in the evaluation documentation.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Use Extreme Scenarios}
\begin{itemize}
\item The absurd may jolt the project into defining limits.
\item Typically, investigate one normal scenario and several extremes, where the boundaries of the requirements are probed.
\item Be open to pursuit promising paths, e.g. with even more extreme scenarios even if you had not initially planned them.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{The Impact of the Project Manager}
\begin{itemize}
\item It is \emph{absolutely vital} that the project manager understands the benefits of the evaluation.
\item The project manager is the lest technical of the project members (?)
\item Perceives pressure from mid-level management to produce
\item Group issues: Do the other project members dare speak up against their project manager?
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Summarise often}
\begin{itemize}
\item Keep the evaluation and the project ``on track''
\item Make sure that found issues are clearly presented and understood.
\end{itemize}
\end{frame}


\begin{frame}[t]
\frametitle{Architecture Transformations}
\begin{itemize}
\item Once you have identified a weak spot in the architecture, you change it.
\subitem \ldots provided, of course, that the ROI of changing the architecture makes it worthwile.
\subitem<2-> \ldots and also provided that you cannot change the requirements instead.
\item Bosch 2000 labels this modification of the architecture as \emph{architecture transformation}.
\item The idea is to modify the architecture while keeping the domain functionality invariant
\subitem thus only impacting the quality attributes of the system.
\item A transformation will improve some quality attributes but may detereorate others.
\end{itemize}

\onslide<3->{
Steps:
\begin{enumerate}
\item Identify which quality requirements that are not yet fulfilled
\item Identify at what components or locations in the architecture is the quality attribute inhibited
\item Select transformation that will solve the identified problem spots in the architecture
\item Transform the architecture accordingly.
\end{enumerate}
}
\end{frame}

\begin{frame}[t]
\frametitle{Types of Transformations}
\includegraphics[width=12cm]{FArchitectureTransformations.pdf}
\end{frame}

\begin{frame}[t]
\frametitle{Types of Transformations}
From Biggest to Smallest:
\begin{scriptsize}
\begin{itemize}
\item Impose Architectural Style
\subitem<2-> Reorganise the entire architecture (of the entire system \emph{or} a specific sub-system)
\subitem<2-> Examples: Pipes-and-Filters, Layered, Model-View-Controller, Blackboard, etc.
\item Impose Architectural Pattern
\subitem<3-> Maintain the components in the architecture and their functionality, but change their behaviour.
\subitem<3-> Examples: Introduce Concurrency, Persistence, Distributed Communication Mechanisms
\item Apply Design Pattern
\subitem<4-> Reorganises a single component, or a small set of components according to the principles of a design pattern.
\subitem<4-> Examples: Facade, Observer, Abstract Factory, Strategy.
\item Convert QR to Functionality
\subitem<5-> Add functionality, not primarily from the application domain.
\subitem<5-> Examples: Exception handling, self-monitoring, undo history.
\end{itemize}
\end{scriptsize}
\end{frame}

\begin{frame}[t]
\frametitle{Summary}
\begin{itemize}
\item There are different times and different purposes for doing architecture evaluation
\item The purpose and the time decides which method to use.
\item Some of the more well-known are SAAM and ATAM for scenario-based evaluations.
\item Researchers from BTH have also made some humble contributions in the area, including QASAR and Experiences from the BTH 4-hour evaluation method.
\item After the evaluation, the architecture should be transformed to address the found issues. Bosch 2000 introduces four ways of transforming the architecture.

\item There are other ways of evaluating (and describing) architectures. Recent endeavours include AADL, which is the focus of the next lecture.
\end{itemize}
\end{frame}


% Transformations

% -----------------------------


%% All of the following is optional and typically not needed. 
%\appendix
%\begin{frame}[allowframebreaks]
%  \frametitle{For Further Reading}
%    
%  \begin{thebibliography}{10}
%    
%  \beamertemplatebookbibitems
%  % Start with overview books.

%  \bibitem{Author1990}
%    A.~Author.
%    \newblock {\em Handbook of Everything}.
%    \newblock Some Press, 1990.
%     
%  \beamertemplatearticlebibitems
%  % Followed by interesting articles. Keep the list short. 

%  \bibitem{Someone2000}
%    S.~Someone.
%    \newblock On this and that.
%    \newblock {\em Journal of This and That}, 2(1):50--100,
%    2000.
%  \end{thebibliography}
%\end{frame}

\end{document}


