\documentclass[10pt]{beamer}

%% Use this for 4 on 1 handouts
%\documentclass[handout]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[landscape, a4paper, border shrink=5mm]

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\def\subitem{\item[\hspace{1.5cm} -]}

\usepackage{graphvizzz}

% Set the presentation mode to BTH
\mode<presentation>
{
	\usetheme{BTH_msv}
	% Comment this if you do not want to reveal the bullets before they are going to be used
	\setbeamercovered{transparent}
}


% Information for the title page

\title[]{Quality Attributes\\and\\Architecture Decisions}
\subtitle{L02 PA1308}
%\date[]{}

\author[Mikael Svahnberg]{Mikael Svahnberg\inst{1}}
\institute[BTH] % (optional, but mostly needed)
{
  \inst{1}%
 Mikael.Svahnberg?bth.se\\
 School of Computing\\
 Blekinge Institute of Technology%
}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%\AtBeginSubsection[]
%{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
%}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 
%\beamerdefaultoverlayspecification{<+->}

\begin{document}

% Titlepage frame
\begin{frame}
  \titlepage
\end{frame}

% ToC frame
% Use \section and \subsection commands to get things into the ToC.
%\begin{frame}
 %\frametitle{Outline}
 % \tableofcontents
%\end{frame}

% -----------------------------
% Your frames goes here
% -----------------------------

\begin{frame}[t]
\frametitle{Quality}

\begin{itemize}
\item What is quality?
\end{itemize}

\end{frame}

\begin{frame}[t]
\frametitle{Terms}

\digraph[scale=0.5]{FQATerms}{
  compound=true;
  ranksep="1";
	q1 [label="Quality"];
	q1->q4 [lhead="cluster_c1"];
	q1->q5 [lhead="cluster_c2"];	
subgraph cluster_c1 {
	label="Requirements";	
	q3 [label="Quality Requirement"];
	q4 [label="Non-Functional Requirement"];
	q4->q3;
}
subgraph cluster_c2 {
	label="System";
	q5 [label="Quality Attribute"]
}
}
\end{frame}

\begin{frame}[t]
\frametitle{Requirement vs Attribute}
\begin{itemize}
\item A quality attribute is always present
\item A quality requirement puts a constraint on an attribute
\item A quality requirement describes a service level of the system (or, more likely, a functional requirement).
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Architecture and Quality Attributes}
\begin{itemize}
\item Functionality is ``easy'' to implement.
\item Quality requirements may \emph{sometimes} have impact on the implementation
\item More often, it impacts the \emph{software structure} (=the software architecture).
\item \ldots And yet, the architecture can only describe a \emph{potential} for achieving a particular quality level.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Examples}

\begin{itemize}[<+->]
\item Usability
\begin{itemize}
\item $\neg$Button layout etc.
\item Certain functions (e.g. undo, data re-use).
\end{itemize}
\item Modifiability
\begin{itemize}
\item How is functionality divided?
\item \ldots In relation to likely change scenarios.
\end{itemize}
\item Performance
\begin{itemize}
\item communication between components
\item division of functionality between components
\item allocation of shared resources
\item $\neg$choice of algorithms
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Levels of Quality Attributes}
\begin{itemize}
\item Business Qualities\footnote{Observe that I use a different order than Bass et al.}
\begin{itemize}
\item Time-to-Market, Cost and Benefit, Projected Lifetime, Targeted market, Rollout schedule, Legacy system integration
\item Also: Product portfolio, Requirements from Society, etc.\footnote{T. Gorschek and A. M. Davis. Requirements engineering: In search of the dependent variables. \emph{Information and Software Technology}, 50(1-2):67-75, 2008.}
\end{itemize}
\item System Quality Attributes
\begin{itemize}
\item Availability, Modifiability, Performance, Security, Testability, Usability
\item ISO 9126: Functionality, Reliability, Usability, Efficiency, Maintainability, Portability
\item There are many more classifications. See e.g. Svahnberg \& Henningsson 2009\footnote{M. Svahnberg and K. Henningsson. Consolidating different views of quality attribute relationships. In
\emph{7th Workshop on Software Quality, proceedings of the International Conference on Software Engineering (ICSE2009)}, pages 46-50, Los Alamitos CA, may 2009. IEEE Computer Society Press.}
\end{itemize}
\item Architecture Qualities
\begin{itemize}
\item Conceptual Integrity, Correctness and Completeness, Buildability
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Achieving Quality Attributes (I)}
\begin{itemize}
\item In order to achieve a a certain level for a quality attribute we need a \emph{controlled process} to lead us towards an architecture \emph{decision}.
\item Quality Attribute Scenarios is one building block for this:
\includegraphics[width=10cm]{FQAScenario.pdf}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Example of Quality Attribute Scenario: Performance}
\includegraphics[width=10cm]{FQAScenarioExample.pdf}
\begin{scriptsize}
\begin{tabular}{ll}
Aspect & Value\\
\hline
Source & Users\\
Stimulus & Initiate transactions: 1000 per minute\\
Artifact & System\\
Environment & Normal mode (c.f. overload mode)\\
Response & Transactions are Processed\\
Response Measure & Latency of 2s (deadline, throughput, jitter, miss rate, data loss, etc)\\
\hline
\end{tabular}
\end{scriptsize}
\end{frame}

\begin{frame}[t]
\frametitle{Achieving Quality Attributes (II)}
\begin{itemize}
\item The next step is to find a solution to a Quality Attribute Scenario.
\item To this, we have \emph{tactics}.
\item A tactic \emph{can be} (but is not limited to) a design pattern, an architecture pattern, or an architectural strategy.
\end{itemize}
\vspace{0.5cm}
Generic:
\vspace{-0.5cm}
\digraph[width=10cm]{FGenericTactic}{
	rankdir=LR;
	Source [shape=doublecircle, label="Source"];
	Target [shape=oval, label="Response Measure"];
	Tactic [shape=rectangle, label="Tactics to control Response"];	
	Source->Tactic [label="Stimulus"];
	Tactic->Target [label="Response"];
}

\vspace{-0.5cm}
Example (Performance):
\vspace{-0.5cm}
\digraph[width=12cm]{FGenericTacticExample}{
	rankdir=LR;
	Source [shape=doublecircle, label="Source"];
	Target [shape=oval, label="Response Measure"];
	Tactic [shape=rectangle, label="Tactics to control Performance"];	
	Source->Tactic [label="Events Arrive"];
	Tactic->Target [label="Response generated within time constraints"];
}

\end{frame}

\begin{frame}[t]
\frametitle{Example:\\Summary of Performance Tactics}
\digraph[height=6.5cm]{FPerformanceTacticsSummary}{
	compound=true;
	rankdir=LR;
	Performance->RD0 [lhead="cluster_RD"];
	Performance->RM0 [lhead="cluster_RM"];
	Performance->RA0 [lhead="cluster_RA"];
	subgraph cluster_RD{
		label="Resource Demand";
		RD0[label="Increase Computational Efficiency"];
		RD1[label="Reduce Computational Overhead"];
		RD2[label="Manage Event Rate"];
		RD3[label="Control Frequency of Sampling"];
	}
	subgraph cluster_RM{
		label="Resource Management";
		RM0[label="Introduce Concurrency"];
		RM1[label="Maintain Multiple Copies"];
		RM2[label="Increase Available Resources"];
	}
	subgraph cluster_RA{
		label="Resource Arbitration";	
		RA0[label="Scheduling Policy"];
	}
}

\vspace{-0.5cm}
\scriptsize{For many more and for other quality attributes, see Bass et al.}
\end{frame}

\begin{frame}[t]
\frametitle{Other Concerns}
\begin{itemize}
\item One may be led to believe that if only the quality requirements are taken care of, the rest will follow.
\item Obviously, this is not the case.
\item Hofmeister et al.\footnote{C. Hofmeister, R. Nord, and D. Soni. \emph{Applied Software Architecture}. Addison-Wesley, Reading MA, 2000.} lists three sources of concerns\footnote{Please note the overlap to the categories from Bass et al.}:
\begin{itemize}
\item Organisational factors:
\only<1>{
\begin{itemize}
\item Management (cf. business qualities above)
\item Staff skills, interests, strenghts, weaknesses
\item Process and development environment
\item Development schedule
\item Development Budget
\end{itemize}
}
\item Technological factors
\only<2>{
\begin{itemize}
\item General-purpose hardware
\item Domain-specific hardware
\item Software technology
\item Architecture technolog
\item Standards
\end{itemize}
}
\item Product factors
\only<3>{
\begin{itemize}
\item Functional Features
\item User Interface
\item Performance
\item Dependability
\item Failure detection, reporting, recovery
\item Service
\item Product cost
\end{itemize}
}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Working with Factor Tables (I)}
\begin{itemize}
\item Identify factors
\item Analyse changeability and flexibility
\item Analyse impact of the factors
\end{itemize}

\begin{scriptsize}
\begin{tabular}{*{3}{p{3.5cm}}}
Factor & Flexibility and Changeability & Impact\\
\hline
{\bf O1: <Factor category>}\\
O1.1: <Factor name>\\
<description of factor> & <What aspects of the factor are flexible or changeable> & <Components affected by the factor or changes to it>\\
\ldots \\
\hline
\end{tabular}
\end{scriptsize}

\only<2>{
\begin{block}{Identify Factors}
Factors that have a ``global influence'', could change during development, are difficult to satisfy, or where you have little experience:
\begin{itemize}
\item Can the factor's influence be localised to one component, or is it distributed across several components?
\item During which stages of development is the factor important?
\item Does the factor require any new expertise or skills?
\end{itemize}
\end{block}
}
\only<3>{
\begin{block}{Analyse Changeability and Flexibility}
Flexibility: What is negotiable about the factor?
\begin{itemize}
\item Is it possible to influence the factor so that it makes architecture development easier? In what way? To what extent?
\end{itemize}
Changeability: What is likely to change in the factor?
\begin{itemize}
\item In what way could the factor change? How likely and how often  will it change? Will the factor be affected by changes in other factors?
\end{itemize}
\end{block}
}
\only<4>{
\begin{block}{Analyse Impact of Factors}
If the factor changes, what else will have to change:
\begin{itemize}
\item Other factors?
\item Components?
\item Design Decisions?
\item Modes of operation in the system?
\end{itemize}
\end{block}
}
\end{frame}

\begin{frame}[t]
\frametitle{Working with Factor Tables (II)}
\begin{itemize}
\item Once you have identified the relevant factors that will influence your architecture, you need to decide how to address them.
\item The factors are probably too many to maintain a decent overview, so we need to simplify.
\item Factors and their changeability are used to identify \emph{Issues}. An issue may arise from:
\begin{itemize}
\item limitations or constraints imposed by factors. For example, agressive development schedule vs requirements overload.
\item the need to reduce the impact of changeability of factors. For example, design the architecture to reduce the cost to porting to other operating systems.
\item difficulties in satisfying product factors. For example, high throughput requirements may overload the CPU.
\item the need to have a common solution to global requirements. For example, error handling and recovery.
\end{itemize}
\item Several factors may affect an issue. Factors may conflict with each other $\rightarrow$ renegotiations.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Factors and Issues, what next?}
\begin{itemize}
\item Develop \emph{Solutions} and \emph{Specific Strategies}
\item These are your documented, motivated, and analysed, \emph{architectural decisions} to solve the issues.
\item Your strategies should address the following goals:
\begin{itemize}
\item Reduce or localise the factor's influence.
\item Reduce the impact of the factor's changeability on the design and other factors.
\item Reduce or localise required areas of expertise or skills.
\item Reduce overall time and effort.
\end{itemize}
\item Once this is done, identify strategies that are related to each other, link them, and ensure that they can work together.
\onslide<2>{
\item So much for Hofmeister et al. I would also add (and require in the assignments):
\begin{itemize}
\item Address the issue with a \emph{software} solution, wherever possible.
\end{itemize}
}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Issue Card}
\begin{tabular}{|p{8cm}|}
\hline
{\bf <Name of the architecture design issue>}\\
\hline
<Description of the issue>\\
{\bf Influencing Factors}\\
<List of the factors that affect this design issue and how>\\
\hline
{\bf Solution}\\
<Discussion of a general solution to the design issue, followed by a list of the associated strategies>\\
{\bf Strategy: \emph{<Name of the strategy>}}\\
<Explanation of the strategy>\\
\hline
{\bf Related Strategies}\\
<References to related strategies and a discussion of how they are related to this design issue>\\
\hline
\end{tabular}

\end{frame}

\begin{frame}[t]
\frametitle{Issue Card Example}
\begin{tabular}{|p{8cm}|}
\hline
{\bf Issue i1: \emph{Automatically Adapt to Input Format}}\\
\hline
The KWIC system can be fed input in many formats, such as xls, doc, rtf, tex, pdf, or txt. The system shall automatically detect the input format (without access to the file name), and process it accordingly. New input formats may be added at runtime.\\
{\bf Influencing Factors}\\
\hspace{0.1cm} P6.4 Adapt to input format\\
\hspace{0.1cm} P6.32 Support Runtime Updates\\
\hline
{\bf Solution}\\
\\
{\bf Strategy: ?}\\ % Pre-parse plugin, Parallell-parse with all available and let them fail, lex-grammar for pre-parsing. Each input format as a module that translates to an internal format. Blackboard pattern.
\hline
{\bf Related Strategies}\\
\\
\hline
\end{tabular}
\end{frame}

\begin{frame}[t]
\frametitle{Bass et al: Attribute-Driven Design}
Bass et al. describes a different way of designing the architecture:
\begin{enumerate}
\item Choose the module to decompose
\item Refine the module according to these steps:
\begin{enumerate}
\item Choose the architectural drivers from quality scenarios and functional requirements
\item Choose an architectural pattern that satisfies the architectural drivers
\item Instantiate modules and allocate functionality
\item Define interfaces of child modules
\item Verify and refine use cases and quality scenarios and make them constraints for the child modules.
\end{enumerate}
\item Repeat for every module that needs further decomposition.
\end{enumerate}
\end{frame}

\begin{frame}[t]
\frametitle{Benefits of each}
\begin{itemize}
\item Hofmeister et al. presents a decision rich methodology, with emphasis on traceability and documentation.
\item Bass et al. comes to a point where you can start designing (making architecture decisions) sooner.
\item Hofmeister et al. takes a wider perspective; Not only can the factors influence the architecture, but they can also influence each other and, as we will see, the architecture can also influence the factors.
\item Bass et al. seems to assume a high-level design of the system beforehand (I may be wrong on this, though).
\item Ultimately, they both land on the same point: \emph{You have to decide how to address your issues or architectural drivers.}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Software Solutions}
\begin{itemize}
\item In a course (or any hypothetical system that is never going to be built), it is often easy to solve issues simply by allowing more or better hardware.
\item In industry, the hardware constraints are \emph{real} and \emph{hard}.
\item For example (using low estimates):
\begin{itemize}
\item Require 1GB more internal memory in the computer = 17 Euro.
\item Ship 1000 units/year = 17 000 Euro.
\item Expected lifespan of system: 10 years = 170 000 Euro.
\item Ensure availability of the right memory modules for the hardware platform for the coming 10 years: lots more.
\end{itemize}
\item Contrast this with one well payed swedish developer working full-time for one year to reduce the memory consumption in software: 75000 Euro (including tax and social fees).
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Whence Strategies?}
\begin{itemize}
\item Architectyre Styles and Patterns
\item Design Patterns
\item Tactics
\onslide<2>{
\item Creativity
\item Experience
}
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Issue Cards and Tactics}
\begin{itemize}
\item An issue card can be viewed as a quality attribute scenario. Example:
\fbox{\begin{minipage}{11cm}
\begin{scriptsize}
The KWIC system can be fed input in many formats, such as xls, doc, rtf, tex, pdf, or txt. The system shall automatically detect the input format (without access to the file name), and process it accordingly. New input formats may be added at runtime.\\
{\bf Influencing Factors}\\
\hspace{0.1cm} P6.4 Adapt to input format\\
\hspace{0.1cm} P6.32 Support Runtime Updates
\end{scriptsize}
\end{minipage}}
\item Source: User or command shell.
\item Stimulus: \onslide<2->{Any of xls, doc, rtf, tex, pdf, or txt.}
\item Environment: \onslide<3->{Runtime Updates, Normal Operations}
\item Response: \onslide<4->{List of KWIC}
\item Response Measure: \onslide<5->{Success or Fail to generate KWIC}
\end{itemize}
\vspace{-0.5cm}
\includegraphics[width=10cm]{FGenericTactic.pdf}
\end{frame}

\begin{frame}[t]
\frametitle{Summary}
\begin{scriptsize}
\begin{itemize}
\item The success of a software system is determined not only by its functionality, but even more so by its \emph{quality}
\subitem Quality is a very wide concept, that has different meanings to different stakeholders.
\subitem Quality attributes are the sub-aspects of quality that a system exhibits.
\subitem The potential to meet many quality attributes is achieved through the software architecture.
\subitem Thus, architecture design methodologies often focus on how to address all the (potentially conflicting) quality requirements.
\subitem This creates a link: Quality Requirement $\rightarrow$ Design Decision $\rightarrow$ Architecture Design $\rightarrow$ Implementation
\subitem \emph{However}, there are more concerns than quality requirements, cf. Hofmeister et al.
\item The first rule of architecture design is to \emph{document your decisions}
\subitem Hofmeister et al. provides one way to do this, through Factors, Issues, and Strategies. Factors and Issues serve as a motivation for your strategies (decisions).
\subitem Bass et al., and their ADD method, provides another way to do this, but leaves much more of the decision rationale open.
\item Knowing how to solve a problem in the architecture is not trivial. You can base yourself on experience or creativity, but there are proven solutions available for you.
\subitem Architecture styles, patterns, and \emph{tactics} are available for you to solve your architectural issues.
\subitem A tactic is an established solution for addressing a particular quality attribute.
\item \fbox{Next Step: Documenting your Architecture, Views and Beyond.}
\end{itemize}
\end{scriptsize}
\end{frame}

% -----------------------------


%% All of the following is optional and typically not needed. 
%\appendix
%\begin{frame}[allowframebreaks]
%  \frametitle{For Further Reading}
%    
%  \begin{thebibliography}{10}
%    
%  \beamertemplatebookbibitems
%  % Start with overview books.

%  \bibitem{Author1990}
%    A.~Author.
%    \newblock {\em Handbook of Everything}.
%    \newblock Some Press, 1990.
%     
%  \beamertemplatearticlebibitems
%  % Followed by interesting articles. Keep the list short. 

%  \bibitem{Someone2000}
%    S.~Someone.
%    \newblock On this and that.
%    \newblock {\em Journal of This and That}, 2(1):50--100,
%    2000.
%  \end{thebibliography}
%\end{frame}

\end{document}


