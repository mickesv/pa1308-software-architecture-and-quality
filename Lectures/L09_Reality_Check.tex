\documentclass{beamer}

%% Use this for 4 on 1 handouts
%\documentclass[handout]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[landscape, a4paper, border shrink=5mm]

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphvizzz}
\def\subitem{\item[\hspace{1.5cm} -]}

% Set the presentation mode to BTH
\mode<presentation>
{
	\usetheme{BTH_msv}
	% Comment this if you do not want to reveal the bullets before they are going to be used
	\setbeamercovered{transparent}
}


% Information for the title page

\title[]{Reality Check}
\subtitle{L09 PA1308}
%\date[]{}

\author[Mikael Svahnberg]{Mikael Svahnberg\inst{1}}
\institute[BTH] % (optional, but mostly needed)
{
  \inst{1}%
 Mikael.Svahnberg@bth.se\\
 School of Computing\\
 Blekinge Institute of Technology%
}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%\AtBeginSubsection[]
%{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
%}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 
%\beamerdefaultoverlayspecification{<+->}

\begin{document}

% Titlepage frame
\begin{frame}
  \titlepage
\end{frame}

% ToC frame
% Use \section and \subsection commands to get things into the ToC.
%\begin{frame}
 %\frametitle{Outline}
 % \tableofcontents
%\end{frame}

% -----------------------------
% Your frames goes here
% -----------------------------

\begin{frame}[t]
\frametitle{Caveat}
\begin{itemize}
\item I will \emph{not} be able to tell you anything specific about any company's architectures.
\item To be on the safe side, I will not even mention the companies by name or domain.
\item This presentation is based on a merge of experiences gained at several companies.
\subitem All companies may not be exposed to all challenges.
\subitem Most are exposed to most, however.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Development Scale I}
\begin{itemize}
\item Rich hardware environment; many nodes, of many different types of hardware.
\item Rich software environment; many collaborating and/or communicating software systems
\item Large software systems; several million lines of code per system, at the smallest.
\end{itemize}

Strategy: Modular design \ldots
\begin{itemize}
\item in order to manage the size.\\(Concerns for the Module view)
\item where the effect of different hardware is localized\\(Execution view $\rightarrow$ Module view)
\item where the effect of interacting software is localized\\(Execution view $\rightarrow$ Module view)
\end{itemize}
\end{frame}

\begin{frame}[t, shrink]
\frametitle{Development Scale II}
\begin{itemize}
\item \emph{Many} developers
\subitem In multiple sites, in multiple countries
\end{itemize}

Further complications:
\begin{itemize}
\item One feature may span several software systems, and/or several modules, and/or several development teams.
\item In order to implement one feature, changes may be required in another part of the system (not owned by you), so you need to deal with change requests.
\end{itemize}

Strategies:
\begin{itemize}
\item Modular design, to keep development teams small.
\item Static code ownership vs rotating code ownership?
\item Independent development (scrum teams)
\subitem Localise changes (including well-defined interfaces).
\subitem Distributed (but synchronised) backlog, or other mechanisms to deal with change requests.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Concurrent Development}

\begin{columns}[t]
\column{5cm}
GitFlow\\
\includegraphics[height=6.5cm]{FGitFlow.pdf}

\column{5cm}
\begin{itemize}
\item {\tiny\url{http://nvie.com/posts/a-successful-git-branching-model/}}
\item How can the \emph{architecture} support development of many features concurrently?
\item How can the \emph{architecture} support merging of features and releases?
\end{itemize}
\end{columns}
\end{frame}

% Should be something here about architectural choices for Concurrent development

\begin{frame}[t]
\frametitle{Customer Adaptations I}

\begin{columns}[t]
\column{5cm}
\only<1>{GitFlow\\
\includegraphics[height=6.5cm]{FGitFlow.pdf}}
\only<2>{Product Customisations
\includegraphics[height=5cm]{FPCFrameworkIdeal.pdf}}

\column{5cm}
\begin{itemize}
\item Customer adaptations creates yet another CVS branch.
\item Unlike feature development, customer adaptations may incur modifications in the entire system.
\item<2-> When should you integrate a product customisation?
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[t]
\frametitle{Customer Adaptations II}
\begin{columns}[t]
\column{2cm}

\includegraphics[height=2cm]{FPCFrameworkIdeal.pdf}

\column{9cm}
\begin{scriptsize}
\begin{itemize}
\item Position within the PC framework determines your choice of integration level
\item Most loosely coupled: Keep the PC as a separate CVS branch
\item Most tightly coupled: Integrate functionality in main line (at next merge point)
\item If you have many PC's of low commonality in the same part of the architecture, you can create a \emph{hotspot}.
\subitem A hotspot localises a particular type of change to a small set of components.
\subitem May introduce e.g. scripting languages to deal with the hostpot.
\subitem In other words, you \emph{defer binding} of a variation point.
\item Confounding factor: \emph{build flags}.
\end{itemize}
\end{scriptsize}
\end{columns}
\end{frame}

\begin{frame}[t]
\frametitle{Build Flags}
\begin{itemize}
\item Probably the most common way to deal with variability
\item In its simplest form it is a \texttt{\#define} - statement
\item There are, however, several challenges:
\subitem Overuse of build flags: Every PC, almost every feature gets a build flag.
\only<1-2>{
\begin{block}<2->{Example}
An informal count at a company revealed that you had uptowards 32000 available configurations of the system. It is unknown which flags that are actually set together.
\end{block}
}
\subitem One flag sets many variation points: One flag may be used in many places in the code.
\subitem Include-or-exclude when set: Is the flag used with \texttt{\#ifdef} or \texttt{\#ifndef} or a combination?
\item<3-> May also impact architecture: include or exclude components, include or exclude interactions between components.

\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Variability I}
\begin{itemize}
\item Actually, build flags and script languages are just two ways of dealing with variability\footnote{M. Svahnberg, J. Van Gurp, and J. Bosch. A taxonomy of variability realization techniques.\emph{Software Practice and Experience}, 35(8):705--754, 2005.}
\item Generic workflow:
\digraph[width=12cm]{FVarWorkflow}{
rankdir=LR;
n1	[label="Identify Variability"];
n2[label="Constrain Variability"];
n3[label="Implement Variability"];
n4[label="Manage Variability"];
n1->n2->n3->n4;
}
\item States of a variant feature:
\digraph[width=12cm]{FVarStates}{
rankdir=LR;
n1[label="Identified"];
n2[label="Implicit"];
n3[label="Introduced"];
n4[label="Populated"];
n5[label="Bound"];
n1->n2->n3->n4->n5;
}
\item Thus, a variant feature can be \emph{introduced} at one time, \emph{populated} at another, and \emph{bound} at a third.
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Variability II}
\begin{itemize}
\item Introduction time: Architecture design, detailed design, implementation, compilation, linking
\item Population time: Depends on the variation point. %Population can be done implicitly or explicitly.
\item Binding time: Product architecture derivation (e.g. CVS checkout), Compilation, Linking, Runtime. %Binding can be done internally or externally.
\vspace{0.2cm}
\item[$\rightarrow$] Depending on your choice of variability mechanism you may need more or less support from your architecture.
\subitem Are your components of the right size to facilitate your variability mechanisms?
\subitem Are the interactions between components such that you can reduce the number of variation points? Especially to variant components.
\subitem \ldots
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Reuse}
\begin{itemize}
\item How does your architecture (and organisation) support reuse?
\item How are variation points managed to enable reuse of components?
\item Special case of reuse: Software Product Lines

\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Summary}
\begin{itemize}
\item The architecture is not developed alone.
\item Especially organisational factors influence your architectural choices.
\subitem Number of developers
\subitem Conway's law\footnote{M. Conway. How do committees invent. \emph{Datamation}, 14(4):28--31, 1968.}
\subitem Size of features
\subitem Code branching
\subitem Number of customers, and customer adaptations
\subitem Choices of variability mechanisms
\subitem Company policies regarding reuse of software artefacts.
\end{itemize}
\end{frame}

% Lost puppies
% -------------------

%\begin{frame}[t]
%\frametitle{Variability III}
%Thus:
%\digraph[width=12cm]{FVarExample}{
%rankdir=LR; ordering=out;
%ScI[label="Scripting Introduced"];
%ScP[label="Scripting Populated"];
%ScB[label="Scripting Bound"];
%subgraph cluster_flow{
%n1[label="Architecture Design"];
%n2[label="Product Architecture Derivation"];
%n3[label="Implementation"];
%n4[label="Compilation"];
%n5[label="Linking"];
%n6[label="Runtime Linking"];
%n7[label="Runtime"];
%n1->n2->n3->n4->n5->n6->n7;}
%ScI->n1; ScP->n7; ScB->n7;
%BfI[label="Build Flag Introduced"];
%BfP[label="Build Flag Populated"];
%BfB[label="Build Flag Bound"];
%BfI->n3; BfP->n3; BfB->n4;
%}
%%subgraph cluster_scripting{
%%subgraph cluster_buildflags{
%\end{frame}

%\begin{frame}[fragile, t]
%\frametitle{Concurrent Development II}
%\begin{semiverbatim}\scriptsize
%\onslide<1->{                                       Merge Point}
%\onslide<1->{                                             V}
%\onslide<1->{----------Line Development ------------------------------------------------------------------>}
%\onslide<1->{       \\------Feature Development, Bug Fix etc---/}
%\onslide<1->{}
%\onslide<1->{       \\---------Release--------------->             .../}
%\onslide<1->{             \\---Bug fix ---/}
%\onslide<2->{}
%\onslide<2->{    \\---------Customer Adaptations------>        .../}
%
%\end{semiverbatim}
%\begin{itemize}
%\item<2-> Unlike feature development, customer adaptations may incur modifications in the entire system.
%\end{itemize}
%\end{frame}



% -----------------------------


%% All of the following is optional and typically not needed. 
%\appendix
%\begin{frame}[allowframebreaks]
%  \frametitle{For Further Reading}
%    
%  \begin{thebibliography}{10}
%    
%  \beamertemplatebookbibitems
%  % Start with overview books.

%  \bibitem{Author1990}
%    A.~Author.
%    \newblock {\em Handbook of Everything}.
%    \newblock Some Press, 1990.
%     
%  \beamertemplatearticlebibitems
%  % Followed by interesting articles. Keep the list short. 

%  \bibitem{Someone2000}
%    S.~Someone.
%    \newblock On this and that.
%    \newblock {\em Journal of This and That}, 2(1):50--100,
%    2000.
%  \end{thebibliography}
%\end{frame}

\end{document}


