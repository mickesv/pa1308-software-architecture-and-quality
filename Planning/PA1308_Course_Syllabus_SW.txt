PA1308 KURSPLAN

KURSPLAN
Programvaruarkitektur och kvalitet
Software Architectures and Quality

7,5 högskolepoäng (7,5 ECTS credit points)
Kurskod: PA1308
Nivå: Grundnivå
Fördjupning: G2F
Utbildningsområde: Teknik
Ämnesgrupp: Datateknik
Huvudområde: Programvaruteknik
Version: 5
Gäller från: 2008-06-18
Fastställd: 2009-11-01
Ersätter kursplan fastställd: 2008-05-13

1. Kursens benämning och omfattning

Kursen benämns Programvaruarkitektur och kvalitet / Software Architectures and Quality och omfattar 7,5 högskolepoäng. En högskolepoäng motsvarar en poäng i European Credit Transfer System (ECTS).

2. Beslut om fastställande av kursplan

Denna kursplan har inrättats och fastställts av 2008-06-18. Denna revision gäller från 2008-06-18.

Ersätter PA1301.

Dnr: TEK56-194/08

3. Syfte

Programvaruarkitekturer är ett livsnödvändigt tekniskt koncept i modern storskalig programvaruutveckling som tjänar flera syften; man planerar utvecklingsresurser baserat på arkitekturen, man analyserar problemdomänen ur ett flertal perspektiv med hjälp av arkitekturen, och man abstraherar stora mängder information för att kunna få en användbar överblick med hjälp av arkitekturen. Dessutom är arkitekturen, och hur man väljer att konstruera arkitekturen, en nyckelkomponent för att planera och åstadkomma en viss kvalitetsnivå i ett system, vilket därmed bestämmer hur framgångsrikt systemet kommer vara.

I den här kursen förväntas studenten införskaffa detaljerade kunskaper om programvaruarkitekturer och programvarukvalitet och, i synnerhet, hur det senare påverkas av det förra.

Vidare förväntas studenten införskaffa en förståelse av hur man konstrurerar en programvaruarkitektur baserat på moderna metoder och idéer såsom designmönster, objektorienterade ramverk, och komponentbaserad programvaruteknik som tar hänsyn till den planerade produkten, den omgivande teknologin och den utvecklande organisationen på ett sätt som skapar långlivade och hållbara system med en planerad och predikterbar kvalitetsnivå.

4. Innehåll

Kursen omfattar följande moment:
@@ Kvalitetsaspekter i programvara och programvaruarkitekturer
@@ Arkitekturstilar, språk och mönster
@@ Metoder för arkitekturdesign och utvärdering
@@ Komponentbaserad programvaruteknik.

5. Mål

Efter genomförd kurs skall studenten:
@@ tydligt kunna uttrycka en ingående insikt i området programvaruarkitekturer (standarder, nyckelkoncept, definitioner på programvarukvalitet, osv.), och kunna namnge och beskriva ett antal nyckelkomponenter i området.
@@ tydligt kunna uttrycka en ingående kunskap av kvalitet i programvara, och hur detta realiseras i kvantifierbara mål.
@@ oberoende, både på en teoretisk nivå och i praktik, kunna välja mellan ett antal arkitekturstilar, språk och mönster beroende på kravbilden, och kunna skilja på dem.
@@ noggrant och med en uppmärksamhet på detaljer kunna skapa och dokumentera en programvaruarkitektur som består av flera olika vyer och hanterar flera olika typer av angelägenheter.

6. Generella förmågor

Följande generella förmågor tränas i kursen:
@@ Planering och hantering av tid
@@ Kritiskt tänkande
@@ Lagarbete
@@ Problemlösande
@@ Analytisk förmåga

7. Lärande och undervisning

Kursen organiseras runt ett antal föreläsningar där studenterna förväntas delta aktivt genom att diskutera, ifrågasätta, och bidra med egna erfarenheter. Föreläsningarna ges tidigt i kursen för att ge en solid grund för de efterföljande uppgifterna. Uppgifterna är konstruerade för att hjälpa studenterna att reflektera över tidigare erfarenheter, literatur och forskningsartiklar, samt att relatera dessa med varandra.

Detta ger en blandning av state-of-the-art som beskrivet i forskningsartiklar, och state-of-practice som uppvisat av personliga erfarenjeter, kurslitteraturen, och industristudier. Studenterna förväntas också reflektera över vad som kan göras bättre, dvs. processförbättring.

Undervisningen bedrivs på engelska.

8. Bedömning och examination

Examinationsmoment för kursen

-------------------------------------------------
Kod	Benämning	Omfattning	Betyg
-------------------------------------------------
	Uppgift 1 	1,5 hp 		F-A
	Uppgift 2 	1,5 hp 		F-A
	Uppgift 3 	1,5 hp 		F-A
	Uppgift 4 	1,5 hp 		F-A
	Uppgift 5 	1,5 hp 		F-A
-------------------------------------------------
Kursen bedöms med betygen F Otillräckligt, FX Otillräckligt, E Tillräckligt, D Tillfredsställande, C Bra, B Mycket bra eller A Utmärkt. Examinationen består av ett antal uppgifter, varav minst en individuell, som vägs samman till ett totalbetyg på kursen. Normalt används ett genomsnitt av alla uppgifter, men enskilda extrema värden i någon riktning kan användas för att kvalificera betyget ytterligare.

9. Kursutvärdering

Kursansvarig ansvarar för att studenternas synpunkter på kursen systematiskt och regelbundet inhämtas och att resultaten av utvärderingar i olika former påverkar kursens utformning och utveckling.

10. Förkunskapskrav

Studenten måste ha klarat av totalt 30 högskolepoäng från endera av följande kurser: Objektoriented systemutveckling, 7,5 högskolepoäng, Datastrukturer och algoritmer, 7,5 högskolepoäng, Databasteknik, 7,5 högskolepoäng, Datakommunikation, 7,5 högskolepoäng, Realtidssystem, 7,5 högskolepoäng, Operativsystem och distribuerande system, 7,5 högskolepoäng
11. Utbildningsområde och huvudområde

Kursen tillhör utbildningsområdet teknik och ingår i huvudområdet Programvaruteknik.

12. Begränsningar i examen

Kursen kan inte ingå i examen med annan kurs, vars innehåll helt eller delvis överensstämmer med innehållet i denna kurs.

13. Kurslitteratur och övriga läromedel

1. Software Architecture in Practice, Second Edition
Författare: Bass, L., Clements, P., and Kazman, R.
Förlag: Addison-Wesley
Utgiven: 2012
Antal sidor: 640
ISBN10: 0321815734
ISBN13: 978-0321815736

Referenslitteratur

1. Zen and the art of Motorcycle Maintenance
Författare: R.M. Pirsig
Förlag: William Morrow
Utgiven: 1974
Antal sidor: 412
ISBN-10: 0688002307
ISBN-13: 978-0688002305

2. Pattern-Oriented Software Architecture Volume 1: A System of Patterns
Författare: F. Buschmann, R. Meunier, H. Rohnert, P. Sommerlad, M. Stal
Förlag: John Wiley
Utgiven: 1996
Antal sidor: 476
ISBN-10: 0471958697
ISBN-13: 978-0471958697

3. Patterns of Enterprise Application Architecture
Författare: M.J. Fowler
Förlag: Addison-Wesley
Utgiven: 2003
Antal sidor: 530
ISBN-10: 0321127420
ISBN-13: 978-0321127426

4. Lean Architecture: For Agile Software Development
Författare: J.O. Coplien, G. Bjørnvik
Förlag: John Wiley
Utgiven: 2010
Antal sidor: 350
ISBN-10: 0470684208
ISBN-13: 978-0470684207

