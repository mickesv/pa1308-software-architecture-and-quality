\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{cite}
\usepackage{booktabs}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[utf8x]{inputenc}
%\usepackage[swedish]{babel}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}

\title{PA1308 Seminar - Peer Evaluation of Architecture}
\author{Michael Unterkalmsteiner. Mikael Svahnberg, Nauman bin Ali\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
\{mun,msv,nal\}@bth.se}
%\date{}                  % Activate to display a given date or no date

\begin{document}
\maketitle
\begin{abstract}
As part of the assignments in the course Software Architecture and Quality at Blekinge Institute of Technology, you are expected to develop a software architecture for a given software system. Integrated into this development process is a four hour seminar where you do a peer evaluation of another software architecture, and get your own architecture evaluated in return. This document describes the preparation, procedure and expected outcome of the peer evaluation seminar.
\end{abstract}

\section{Introduction}
The seminar has several purposes:
\begin{itemize}
 \item practicing to reason, communicate and argue in software
architecture terms
 \item exercising the assessment of the strengths and weaknesses of a software
architecture, in terms of its ability to fulfill quality requirements
 \item developing and presenting constructive criticism on a software
architecture
 \item learning and applying a simple, but effective, architecture evaluation
method
 \item preparing architecture documentation in a timely and
appropriate-for-the-task manner
 \item learning to absorb, digest and apply feedback 
\end{itemize}

You are expected to perform an architecture evaluation of a peer groups
architecture using the BTH architecture evaluation method (see
Table~\ref{tab:EvaluationSteps}). You are expected to conduct this evaluation in
groups of four, together with the creators of the architecture. 

\section{Seminar preparation}

\begin{enumerate}
\item Read the course book for more information on software architecture evaluations.
\item Read Svahnberg \& Mårtensson 2007\footnote{M. Svahnberg and F. Mårtensson. Six years of evaluating software architectures in student projects. \emph{The Journal of Systems \& Software}, 80(11):1893--1901, 2007.}.
\item Work through the self-study package on Architecture Evaluations.
\item Identify a peer group of your choice, and send the documentation (assignment 1) of your architecture AT LEAST 3 DAYS before the seminar to all group members.
\item Create a stub document, see Figure \ref{F:DocumentStub} for example headings.
\item Decide who will be documenting the evaluation meeting, who will be the timekeeper, and who will be chairing the meeting. 
\end{enumerate}

As you receive the documentation from your peer group, prepare for the
architecture evaluation. Even though you are working on the same system
description as your peer group, pay particular attention on the made
assumptions as they likely differ from your own assumptions. Study the
identified factors, issues and strategies. Even though some strategies may not
be visible in the conceptual view of the architecture, prepare to ask questions
related to their implementation in the module or execution view.

{\bf Make sure} that you get the documentation from your peer group well in time for the evaluation seminar.

\begin{figure}[bth]
  \centering
  \begin{scriptsize}
  \begin{tabular}{|p{10cm}|}
    \hline
    {\large\bf Architecture Assessment}\\
    Project:\\
    Date:\\
    Members and e-mail addresses of the evaluating team:\\
    \\
    Members and e-mail addresses of the evaluated team:\\
    \\
    {\bf 1. Introduction}\\
    {\it -- Describe the system and the evaluation here --}\\
    \\
    {\bf 2. Quality Requirements}\\
    {\it -- List the most important quality requirements under each category --}\\
    2.1 Performance / Efficiency\\
    2.2 Compatibility\\
    2.3 Usability\\
    2.4 Reliability\\
    2.5 Security\\
    2.6 Maintainability\\
    2.7 Portability\\
    \\
    {\bf 3. Selected Quality Requirements}\\
    {\it -- Pick the three most important quality requirements from above --}\\
    \\
    {\bf 4. Scenarios}\\
    4.1 Requirement 1\\
    {\it -- Scenarios --}\\
    4.2 Requirement 2\\
    {\it -- Scenarios --}\\
    4.3 Requirement 3\\
    {\it -- Scenarios --}\\
    \\
    {\bf 5. Architecture Presentation}\\
    {\it -- A brief summary of the architecture as presented --}\\
    \\
    {\bf 6. Evaluation}\\
    6.1 Scenario 1\\
    {\it -- Summary of the evaluation and the found issues --}\\
    6.2 Scenario 2\\
    {\it -- Summary of the evaluation and the found issues --}\\
    6.3 Scenario 3\\
    {\it -- Summary of the evaluation and the found issues --}\\
    6.4 Scenario 4\\
    {\it -- Summary of the evaluation and the found issues --}\\
    6.5 Scenario 5\\
    {\it -- Summary of the evaluation and the found issues --}\\
    6.6 Scenario 6\\
    {\it -- Summary of the evaluation and the found issues --}\\
    \\
    {\bf 7. Summary}\\
    {\it -- Summarise the evaluation and the most important issues found --}\\
    \\
    \hline
  \end{tabular}
  \end{scriptsize}
  \caption{Example Headings for Evaluation Document}
  \label{F:DocumentStub}
\end{figure}

\subsection{Relevant study packages}
\begin{itemize}
\item Architecture Evaluation
\end{itemize}


\section{During the seminar}
\begin{enumerate}
\item Prepare any computer support (e.g. shared documents) before the seminar.
\item Read the evaluation instructions before the seminar.
\item Be on time. If you arrive late or have to leave early, communicate this to your peer
group and arrange.
\end{enumerate}

Table~\ref{tab:EvaluationSteps} illustrates the steps of the evaluation method
your are going to apply during the seminar. Keep in mind that during the
evaluation your group will act both as ``owners'' (of your architecture) and
``evaluators'' (of your peer groups' architecture). In steps 2 and 3
you elicit one set of quality requirements and scenarios which you ``play'' on
both architectures in step 6. The ``owners'' shall document the feedback they
receive from the ``evaluators''.

\begin{itemize}
\item During the seminar, instructors will be available to answer questions.
\item Only use one laptop per team, you have one appointed team member to document the evaluation.
\item The quality requirements should be \emph{specific} quality requirements, not generic categories (e.g. as the headings in Figure \ref{F:DocumentStub}.
\item The times in Table \ref{tab:EvaluationSteps} are estimates. You are likely to spend less time on item no 2 and more on items 3 and 6.
\item Do not spend too much time just reading the architecture documentation; it has been sent to you before the seminar and you should already have read it and analysed it.
\item There is a difference between designing an architecture and evaluating it. This goes for both teams. If you are evaluating, avoid imposing your design ideas onto the evaluated system. If you are being evaluated, do not invent solutions to the raised issues as you go just to ``pass'' the evaluation. You are not graded on how well you pass the evaluation, you are graded on how well you are able to take care of the feedback received during the evaluation in your subsequent assignment submissions.
\item Write a brief summary for each scenario during evaluation -- this is your rationale for why you found a particular issue or for why the raised concern is already addressed by the architecture.
\end{itemize}

\begin{table*}
\caption{Evaluation Steps of BTH 4-hour Software Architecture Evaluation Method}
\centering
\begin{tabular}{l>{\raggedright}p{2.5cm}lp{8cm}}\toprule
Step & Name & Time & Description\\ \midrule
1 & Introduction & 10 min & Introduce each other. Introduce the roles you will
have during the evaluation. Document: Group members (indicating the ``owner'' of
the architecture and the ``evaluators''), names and roles of those present, time
and location.\\ 
2 & Identify Quality Requirements & 30 min & Identify and document the six
quality requirements that are most relevant for this system and relevant to
evaluate.\\
3 & Elicit Scenarios & 50 min & Elicit and document scenarios for the three most
important quality requirements. Look for normal cases as well as extreme
cases.\\
4 & Architecture presentation & 2 x 15 min & The ``owners`` of the architecture
present their architecture solution.\\
5 & Break & 20 min & Take a break, get some coffee. Discuss the evaluation so
far. Is the strategy working? What should be changed? Any specific issues
that must be covered? etc.\\
6 & Scenario and Architecture Analysis & 2 x 40 min & Go through each scenario
in turn, see how it will “play” in the architecture, and where the problems or
bottlenecks may be. You may decide to modify the scenarios in order
to stress the architecture and get some interesting results. Document a brief
description of how the architecture addresses each scenario. Summarize each
scenario with a description of the major points found and any issues that need
to be further investigated by the architecture owners.\\
7 & Conclusion & 15 min & Summarize the evaluation. Document: Repeat the most
pressing issues in an executive summary at the end of the evaluation document.\\
\bottomrule
\end{tabular}
\label{tab:EvaluationSteps}
\end{table*}

\section{After the seminar}
\begin{enumerate}
\item Tidy up your document (spell-check, put things under the right headings, etc.)
\item Send your document to your own team as well as the one you partnered with during the seminar.
\item Both teams meet together over coffee (or chai, bordeaux, bovril, or whatever your taste runs to), and do a post-mortem of the evaluation. How did you react when a particular question was asked? How did you experience being evaluated? Could the other team have answered better? Etc.
\end{enumerate}


\section{Outcome of the seminar}
Use the feedback you receive during the seminar to improve the analysis and the
conceptual view you have developed in assignment 1. IMPORTANT: if you change
something in your analysis/architecture, you need to document the rationale for
the change. In assignment 3 you need to document that, e.g. scenario A
(describe shortly the scenario and the evaluation outcome) led to change X in
the factor tables OR caused the introduction of strategy Y OR the removal of
component Z etc. \emph{Unmotivated changes are not acceptable.}

\end{document}
